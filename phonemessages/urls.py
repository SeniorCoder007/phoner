"""phonemessages URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from messages_viewer.views import *
from db_updater.views import *
from django.conf.urls import handler404, handler500

urlpatterns = [
    path('admpanel/', admin.site.urls),
    path('get/',get),
    path('add/',get_messages),
    path('check/',check_messages),
    path('view/',view_messages),
	# guesser
	path('start/',start),
	path('stop/',stop_thread),
	path('getThreads/',get_threads),
	# verify actions
	path('startverifier/',start_verifier),
	path('resetverify',reset_verify),
	path('getverifythreads',get_threads_verify),
	path('stopverify',stop_thread_verify),
	# panel
	path('updateget',get_details_update),
	path('panel',control_panel),
	#filter
	path('print/',print_page),
	path('custprint/',custom_print),
	path('pdf/',filter_ui),
	path('reset/',reset),
	path('custpdf',custom_filter),
	path('checkcust',check_status_cust),
	path('getfile',get_file),
	#remove duplicates
	path('removedup',remove_dup),
	#proxy
	path('setproxy',setproxy),
	path('fix',get_addresses),
	#ViewProtection
	path('viewprotection',view_protection),
	path('verifyprot',verify_protection),
	#selector urls
	path('',include('selector.urls'))


]

handler404 = error_404
