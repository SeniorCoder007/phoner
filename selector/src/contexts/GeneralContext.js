import React,{useState, createContext } from 'react';


export const GeneralContext = createContext();

const DataProvider = (props) => {
	const [Data,setData] = useState({
		selected : {

		},
		facs:[],
		all:
			{
			
		},
		loading:false
		

	});
	//const [Data,setData] = useState(['1','2']);

	return (
		<GeneralContext.Provider value={[Data,setData]}>

			{props.children}

		</GeneralContext.Provider>
	)
}




export {DataProvider};