import React, {useContext} from 'react';
import {GeneralContext} from '../contexts/GeneralContext';



function Element(props){

	const [Data,setData] = useContext(GeneralContext);


	//console.log(props.idd);

	function checkState(e){
		let t = e.currentTarget;
		//console.log('hello');
		console.log(t.value);
		if (t.checked) {
		let temp = Data.all;
		let temp_s = Data.selected;
		temp_s[t.value] = Data.all[t.value];
		delete temp[t.value];

		let copy_obj = {
			selected : temp_s,
			facs : Data.facs,
			all:temp,
			loading:Data.loading
		}
		console.log(copy_obj);
		setData(copy_obj);
	} else {
		let temp = Data.all;
		let temp_s = Data.selected;
		temp[t.value] = Data.selected[t.value];
		delete temp_s[t.value];

		let copy_obj = {
			selected : temp_s,
			facs : Data.facs,
			all:temp,
			loading:Data.loading
		}
		console.log(copy_obj);
		setData(copy_obj);
	}
	}

	function trigger(e){
		let t = e.currentTarget;
		//console.log('hello');
		console.log(t.value);
		if (t.checked) {
			let copy_obj = {
				selected :{...Data.selected,...Data.all} ,
				facs : Data.facs,
				all:{},
				loading:Data.loading
			}
			setData(copy_obj)

		} else {
			let copy_obj = {
				selected : {},
				facs : Data.facs,
				all:Data.selected,
				loading:Data.loading
			}
			setData(copy_obj)

		}
	}

	function retElem(t){
		if (t == 0){
			return <li><span>{props.name}</span><span>{props.age}</span> <span>{props.sex}</span> <span>{props.race}</span><input type='checkbox' onClick={trigger} value={props.idd} checked={props.active}></input>  </li>
		} else {
			return <li><span>{props.name}</span><span>{props.age}</span> <span>{props.sex}</span> <span>{props.race}</span><input type='checkbox' onClick={checkState} value={props.idd} checked={props.active}></input>  </li>
		}
	}


	return (

		retElem(props.type)
	)
}


export default Element;