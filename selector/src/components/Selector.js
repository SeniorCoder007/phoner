import React , {useContext,useEffect} from 'react';
import {GeneralContext} from '../contexts/GeneralContext';
import SelectOptions from './SelectOptions';
import Element from './Element';





function Selector(){
	const [Data,setData] = useContext(GeneralContext);



	function getDataajax(url,funct,idd) {
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
		  if (this.readyState == 4 && this.status == 200) {
			var resp = JSON.parse(this.response)
			if (funct == 0){
				handle_main(resp);
		  }else{
				handle_check(resp,idd)
	
			}
		};}
		console.log(url);
		xhttp.open("GET",url, true);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhttp.send();
	
	}
	
	
	
	
	function custmain(inm){
		console.log("here");
		//let inm = document.getElementsByName('inmates')[0].value;
		let c = document.getElementsByName('out_c')[0].value;
		//let c = '1';
		console.log(inm);
		console.log(c);
		getDataajax('/custpdf?inmates='+inm+'&out_c='+c,0);
	
	}
	
	function start_checker(idd){
		getDataajax('/checkcust?id='+idd,1,idd);
	}
	
	function handle_main(resp){
			if (resp['message'].includes('Processing')){
				let idd = resp['id'];
				start_checker(idd);
	
			}else{
				alert('failed');
			}
	}
	
	function handle_check(resp,idd){
		if (resp['message'] == 'done'){
			window.location = '/getfile?id='+idd
		} else {
			console.log('not ready yet');
	
			start_checker(idd);
		}
	
	}

	function toggleLoading(l){
		setData({
			selected:Data.selected,
			facs:Data.facs,
			all:Data.all,
			loading:l
		})
	}

	const getInmates = (url) => {

		///pdf?dateStart=2012-12-31&dateEnd=2020-08-17&age=18-51&count=10&white=on&black=on&indian=on&asian=on&facility=ald&out_c=3
		//'/selfilter?fac='+fac
		toggleLoading(true);
		fetch(url).then(resp => resp.json()).then(
			r => {
				let temp = {};
				for(let i = 0; i<r.length;i++){
					temp[i] = r[i];
				}
				console.log(temp);
				//alert('check');
				setData({
					selected:{},
					facs:Data.facs,
					all:temp,
					loading:false

				})
			}
		)

	}

	useEffect(() => {
		//alert('here');
		fetch('/getfacs').then(resp =>  resp.json()).then( r => {
			setData({
				selected : Data.selected,
				facs : r,
				all : Data.all,
				loading:Data.loading
			})
		});
		console.log(Data)
	},[] )

	const handleFac = (e) => {
		//let el = e.currentTarget;
		//getInmates(el.value);
		let dates = document.getElementsByName('dateStart')[0].value;
		let date_e = document.getElementsByName('dateEnd')[0].value;
		let age = document.getElementsByName('age')[0].value;
		let num = document.getElementsByName('count')[0].value;
		let white = document.getElementsByName('white')[0].checked ? 'on' : 'off';
		let black = document.getElementsByName('black')[0].checked ? 'on' : 'off';
		let indian = document.getElementsByName('indian')[0].checked ? 'on' : 'off';
		let asian = document.getElementsByName('asian')[0].checked ? 'on' : 'off';

		let fac = document.getElementById('facs').value;

		let url = '/getfilt?dateStart='+dates+'&dateEnd='+date_e+'&age='+age+'&count='+ num + '&white='+white+'&black='+black+'&indian='+indian+'&asian='+asian+'&facility='+fac;
		console.log(url);
		getInmates(url);

		//console.log(dates+date_e+age+num+white+black+indian+asian+fac);
	}

	function handleprint(){
		let temp = [];
		let cont = Object.keys(Data.selected)
		for (let i = 0;i < cont.length ; i++ ) {
			let key = cont[i];
			console.log(key);
			temp.push(Data.selected[key].inmate_num)

		}
		console.log(temp.join());
		alert('sent to server for preparing result please wait ')
		custmain(temp.join());
	}

	function handleDisplay(){
		if ((Object.keys(Data.all).length > 0 || Object.keys(Data.selected).length > 0) && !Data.loading) {
			return (<div><div className='displayer'>
				
		<div className='main'>
		<h2 className='title'>All Inmates in facility</h2>
			<ul>
				<Element name={"Inmate"} age={"Age"} sex={'Sexe'} race={'Race'} type={0}></Element>
				{Object.keys(Data.all).map(function(k){ return <Element name= {Data.all[k].first_name+ ' ' +Data.all[k].middle_name + ' ' + Data.all[k].last_name } age={Data.all[k].age} sex={Data.all[k].sex} race={Data.all[k].race} idd={k} active={false} /> })}

			</ul>
		</div>
		<aside className='side'>
		<h2 className='title'>Selected Inmates</h2>
			<ul>
			<Element name={"Inmate"} age={"Age"} sex={'Sexe'} race={'Race'}></Element>
		{Object.keys(Data.selected).map(function(k){ return <Element name= {Data.selected[k].first_name+ ' ' +Data.selected[k].middle_name + ' ' + Data.selected[k].last_name } age={Data.selected[k].age} sex={Data.selected[k].sex} race={Data.selected[k].race} idd={k} active={true} /> })}
		</ul>
		</aside>
			
			</div>
			<center>
			<select name="out_c">
<option value=''>default</option>
<option value='1'>pdf</option>
<option value='2'>csv</option>
<option value='3'>both</option>
</select>
<br></br>
				<button className='but' onClick={handleprint}>Print !</button>
			</center>
			</div>
			)
		} else if (Data.loading){
			return <h1 className='subtitle'> Loading ...</h1>
		}
		else {
			return <h1 className='subtitle'> Please Select a Facility </h1>
		}
	}

	return(
		<div className='selector'>
			<div className='filter'>
			
			<input type='date' pattern="[0-9]{2}/[0-9]{2}/[0-9]{4}" name='dateStart' placeholder="The start date in this form : month/day/year"></input>
			
            <input type='date' pattern="[0-9]{2}/[0-9]{2}/[0-9]{4}" name='dateEnd' placeholder="The end date in this form : month/day/year"></input>
          
          
            <input type="text" name="age" placeholder="The age in this form : start - end" id='age' ></input>
			
            <input type="number" name="count" placeholder="Number to be printed" id='age' ></input>
           
			</div>
			<center>
			<div className='subselectors'>
            <input type='checkbox' name='white' ></input> <span >White</span>
			<input type='checkbox' name='black'></input>  <span >Black</span>
			<input type='checkbox' name='indian'></input>  <span >Indian</span>
			<input type='checkbox' name='asian'></input>  <span >Asian</span>
			</div>
			
			
			<select id='facs' >
				<option active value=''>default</option>
			
				{Data.facs.map(d => ( <SelectOptions name={d.desc} value={d.code} /> ))}
			</select>
			<br></br>
			<button className='but' onClick={handleFac}>Filter !</button>
			</center>

			{ handleDisplay() }

			</div>


	)
}


export default Selector;