import React , {Component ,Fragment} from 'react';
import ReactDOM from 'react-dom';
import Selector from './Selector';
import Navbar from './NavBar';
import {DataProvider} from '../contexts/GeneralContext.js';



class App extends Component{
	render() {
		return (
			
			<Fragment>
				<DataProvider>
				<Navbar />
				<Selector />
				</DataProvider>
			</ Fragment>
			
		
		)
	}
}

ReactDOM.render(<App />,document.getElementById('app'));