from django.urls import path
from .views import *


urlpatterns = [
	path('selector',index),
	path('selfilter',get_fac_inmates),
	path('getfacs',get_facs),
	path('getfilt',customFilter),
	path('startcusver',start_cus_verifier)
]


