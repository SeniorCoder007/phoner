from django.shortcuts import render,HttpResponse
import json
from db_updater.models import *
from db_updater.views import *
from django.utils import timezone
from datetime import timedelta

# Create your views here.


def index(request):
	return render(request,'frontend/index.html')



def get_fac_inmates(request):
	if request.method == 'GET':
		fac = request.GET.get('fac',False)
		if fac:
			f = Facilities.objects.filter(code=fac)
			if len(f) > 0:
				f = f[0]
				inms = Inmate.objects.filter(facl_code=fac)
				if len(inms) ==0:
					return HttpResponse(json.dumps([]))
				else:
					d = []
					for u in inms:
						d.append(format_resp_inmate(u))
					return HttpResponse(json.dumps(d))

			else:
				return HttpResponse(json.dumps([]))
		else:
			return HttpResponse(json.dumps([]))




def get_facs(request):
	fs = Facilities.objects.all()
	r = []
	for f in fs:
		r.append(
			{
				'code':f.code,
				'desc':f.description
			}
		)
	return HttpResponse(json.dumps(r))



def customFilter(request):
	if request.method == 'GET':
		name = request.GET.get('name','').upper()
		name_start = request.GET.get('nameStart','').upper()
		surname = request.GET.get('surname','').upper()
		surname_start = request.GET.get('surnameStart','').upper()
		age = request.GET.get('age','')
		date_start = request.GET.get('dateStart','')
		date_start = date_start.replace('-','/')
		print(date_start)
		date_end = request.GET.get('dateEnd','')
		date_end = date_end.replace('-','/')
		print(date_end)
		white = request.GET.get('white','')
		if 'on' in white:
			white = 'white'.lower()
		black = request.GET.get('black','')
		if 'on' in black:
			black = 'black'.lower()
		asian = request.GET.get('asian','')
		if 'on' in asian:
			asian = 'asian'.lower()
		indian = request.GET.get('indian','')
		if 'on' in indian:
			indian = 'American Indian'.lower()
		race = [white,black,asian,indian]
		count = request.GET.get('count','')
		fac_code = request.GET.get('facility','')
		out_c = request.GET.get('out_c','')
		if out_c == '':
			out_c = '1'
		else:
			out_c = str(out_c)
		if '-' in age:
			age_sp = age.split('-')
			age_st = age_sp[0]
			age_end = age_sp[-1]
			age = ''
		else:
			age_st = ''
			age_end = ''
		if name != '':
			if surname != '':
				if age != '':
					users = Inmate.objects.filter(filtered=False,first_name=name,last_name = surname,age=int(age))
					if len(users) != 0:
						ret_list = []
						for user in users:
							ret_list.append(format_resp_inmate(user))
					else:
						return HttpResponse(json.dumps({
							'status':'fail',
							'response':'no inmates found'
						}))
				else:
					users = Inmate.objects.filter(filtered=False,first_name=name,last_name = surname)
					if len(users) != 0:
						ret_list = []
						for user in users:
							ret_list.append(format_resp_inmate(user))
					else:
						return HttpResponse(json.dumps({
							'status':'fail',
							'response':'no inmates found'
						}))
			else:
				if surname_start != '':
					if age != '':
						users = Inmate.objects.filter(filtered=False,first_name=name,last_name__startswith = surname_start,age=int(age))
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
					else:
						users = Inmate.objects.filter(filtered=False,first_name=name,last_name__startswith = surname_start)
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
				else:
					if age != '':
						users = Inmate.objects.filter(filtered=False,first_name=name,age=int(age))
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
					else:
						users = Inmate.objects.filter(filtered=False,first_name=name)
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
		else:
			if name_start != '':
				if surname != '':
					if age != '':
						users = Inmate.objects.filter(filtered=False,first_name__startswith=name_start,last_name = surname,age=int(age))
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
					else:
						users = Inmate.objects.filter(filtered=False,first_name__startswith=name_start,last_name = surname)
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
				else:
					if surname_start != '':
						if age != '':
							users = Inmate.objects.filter(filtered=False,first_name__startswith=name_start,last_name__startswith = surname_start,age=int(age))
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
						else:
							users = Inmate.objects.filter(filtered=False,first_name__startswith=name_start,last_name__startswith = surname_start)
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
					else:
						if age != '':
							users = Inmate.objects.filter(filtered=False,first_name__startswith=name_start,age=int(age))
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
						else:
							users = Inmate.objects.filter(filtered=False,first_name__startswith=name_start)
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
			else:
				if surname != '':
					if age != '':
						users = Inmate.objects.filter(filtered=False,last_name = surname,age=int(age))
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
					else:
						users = Inmate.objects.filter(filtered=False,last_name = surname)
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
				else:
					if surname_start != '':
						if age != '':
							users = Inmate.objects.filter(filtered=False,last_name__startswith = surname_start,age=int(age))
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
						else:
							users = Inmate.objects.filter(filtered=False,last_name__startswith = surname_start)
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
					else:
						if age != '':
							users = Inmate.objects.filter(filtered=False,age=int(age))
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
						else:
							users = Inmate.objects.filter(filtered=False)
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
		print(race)
		if any(check_list(race)) :
			print('doing')
			ret_list = filter_race(race,ret_list)

		if age_st != '' and age_end != '':
			ret_list = filter_age(age_st,age_end,ret_list)
		else:
			pass

		if fac_code != '':
			ret_list = filter_fac(fac_code,ret_list)
			facl_clean = True
		else:
			facl_clean = False
			pass

		if date_start != '' and date_end != '':
			final_list = filter_list(date_start,date_end,ret_list)
		else:
			final_list = ret_list

		print(len(final_list))

		print('count ' + str(count))

		if count != '':
			count = int(count)
			if len(final_list) <= count:
				pass
			else:
				div_count = count - (count%20)
				if div_count == 0:
					final_list = final_list[:count]
				else:
					final_list = final_list[:div_count]
		else:
			pass

		print(len(final_list))
		if  facl_clean:
			final_list = clean_facl_list(final_list)
		#print(final_list)



		#used_list(final_list)
		
		return HttpResponse(json.dumps(final_list))
	else:
		return HttpResponse('Use get requests')


	


def generate_threads_cus_verify(count):
	try:
		#proxies = renew_proxies()
		some_day_last_week = timezone.now().date() - timedelta(days=7) 
		clear_threads_ver(count)
		fnames = Inmate.objects.filter(verified=False,lastverified__lt=some_day_last_week)

		if  len(fnames) != 0:
			pass
		else:
			fnames_test = Inmate.objects.filter(lastverified__lt=some_day_last_week)
			if len(fnames_test) == 0:
				return [False,'no inmates yet']
			else:
				reset_internally_verify()
				return [False,'rerun in a moment']
		rate = len(fnames) // count
		print(rate)
		for y in range(count):
			if rate == len(fnames):
				print('here')
				partial_list = fnames
			elif y == (count-1):
				partial_list = fnames[rate*y:]
			else:
				partial_list = fnames[rate*y:rate*(y+1)]
			#proxy = '108.59.14.200:13152'
			#proxy = '5.79.66.2:13200'
			s = ProxySet.objects.all()[0]
			p = random.choice(Proxies.objects.filter(idd=s.proxy))
			proxy = f"{p.host}:{str(p.port)}"
			#proxies.remove(proxy)
			bt = threading.Thread(target=update_verify,args=(partial_list,str(y),proxy))
			bt.daemon = True
			bt.name = 'v-'+str(y)
			bt.start()
		return [True,str(y)]
	except Exception as e:
		raise e

def start_cus_verifier(request):
	try:
		
		count = request.GET.get('count','1')
		stop_second()
		resp = generate_threads_cus_verify(int(count))
		#resp = [True,'this test']
		print(resp)
		if resp[0]:
			return HttpResponse(json.dumps({
				'status':'runing',
				'message':resp[-1]
			}))
		else:
			return HttpResponse(json.dumps({
			'status':'failed',
			'message':resp[-1]
		}))

	except Exception as e:
		print(str(e))
		return HttpResponse(json.dumps({
			'status':'failed'
		}))