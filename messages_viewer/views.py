from django.shortcuts import render,HttpResponse
from phonemessages.settings import *
from signalwire.rest import Client as signalwire_client
from .models import *
import json
import threading
import time
import random
# Create your views here.

messages = {}

"""returned_data = {
		'sender':phone_num,
		'messages':messages_list
		}
		return render(request,'viewer.html',returned_data)"""

def message_getter(phone,obj):
	client = signalwire_client(project_key, token, signalwire_space_url = space_url)
	messages_list = client.messages.list(from_=phone)

	messages[phone] = messages_list
	obj.key = str(random.randrange(1000,9999999))
	obj.ready = True
	obj.save()

def view_messages(request):
	res = check_protection(request)
	if res:
		pass
	else:
		return redirect('/viewprotection')
	phone_num = request.GET.get('key','')
	if phone_num != '':
		obj = ResultNotification.objects.filter(key=phone_num)
		if len(obj) == 0:
			return HttpResponse('no task in queue')
		else:
			msgs = messages[obj[0].number]
			returned_data = {
			'sender':phone_num,
			'messages':msgs
			}
			return render(request,'viewer.html',returned_data)
	else:
		return HttpResponse('no params')



def get_messages(request):
	res = check_protection(request)
	if res:
		pass
	else:
		return redirect('/viewprotection')
	phone_num = request.GET.get('number','')
	print(phone_num)
	if phone_num != '':
		if '+' in phone_num:
			pass
		else:
			phone_num = phone_num.strip()
			phone_num = '+' + str(phone_num)
		obj = ResultNotification.objects.create(number=phone_num)
		obj.save()
		bt = threading.Thread(target=message_getter,args=(phone_num,obj))
		bt.daemon = True
		bt.start()
		return HttpResponse(json.dumps({
			'status':'success'
		}))

	else:
		return HttpResponse(json.dumps({
			'status':'failed'
		}))


def check_messages(request):
	res = check_protection(request)
	if res:
		pass
	else:
		return redirect('/viewprotection')
	time.sleep(4)
	phone_num = request.GET.get('number','')

	if phone_num != '':
		if '+' in phone_num:
			pass
		else:
			phone_num = phone_num.strip()
			phone_num = '+' + str(phone_num)
		obj = ResultNotification.objects.filter(number = phone_num)
		if len(obj) == 0:
			return HttpResponse(json.dumps({
				'error':'no tasks in queue'
			}))
		else:
			obj = obj[0]
		if obj.ready:

			return HttpResponse(json.dumps({
				'status':'ready',
				'key':obj.key
			}))
		else:
			return HttpResponse(json.dumps({
				'status':'not yet'
			}))




def get(request):
	res = check_protection(request)
	if res:
		pass
	else:
		return redirect('/viewprotection')
	return render(request,'main.html')
