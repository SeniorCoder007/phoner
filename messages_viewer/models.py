from django.db import models

# Create your models here.


class ResultNotification(models.Model):
	number = models.CharField(max_length=255)
	ready = models.BooleanField(default=False)
	key = models.CharField(max_length=255,default='')

	def __str__(self):
		return self.number
