from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Inmate)
admin.site.register(Names)
admin.site.register(Surnames)
admin.site.register(Renewer)
admin.site.register(Working_threads)
admin.site.register(verify_threads)
admin.site.register(Tracker)
admin.site.register(custInm)
admin.site.register(ProxySet)
admin.site.register(Proxies)
admin.site.register(ViewProtection)
admin.site.register(Facilities)
