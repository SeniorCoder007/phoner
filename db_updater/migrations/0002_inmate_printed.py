# Generated by Django 2.2.3 on 2019-07-31 15:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('db_updater', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='inmate',
            name='printed',
            field=models.BooleanField(default=False),
        ),
    ]
