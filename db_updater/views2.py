from django.shortcuts import render,HttpResponse
from .models import *
import json
import os
import threading
import time
from datetime import datetime
import requests as req
import random
from bs4 import BeautifulSoup
import re
from django.http import FileResponse
from reportlab.pdfgen import canvas
import io
import labels
from reportlab.graphics import shapes
import csv



username = 'lum-customer-hl_2c924221-zone-zone1'
password = 'idrx4w6pqq1s'
port = 22225
id_list = []

verify_id_list = []

# Create your views here.

#thread working session


def get_inmate_data(name,surname,idd,proxy):
	try:
		global stop
		global message
		headers = {
	'user-agent' : 'Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0',
}
		session_id = random.random()
		#sess_proxy = {'http': 'socks5h://'+proxy,'https': 'socks5h://'+proxy}
		sess_proxy = {'http':'http://'+proxy,'https':'https://'+proxy}
		main_query  = 'https://www.bop.gov/PublicInfo/execute/inmateloc?todo=query&output=json&inmateNum=&nameFirst={0}&nameMiddle=&nameLast={1}&age=&race=&sex='
		first_query = 'https://www.bop.gov/PublicInfo/execute/inmateloc?todo=query&output=json&inmateNum={0}&inmateNumType=IRN'
		second_query = 'https://www.bop.gov/PublicInfo/execute/phyloc?todo=query&output=json&code={0}'
		nm_resp = req.get(main_query.format(name,surname),headers=headers,proxies = sess_proxy)
		task = Working_threads.objects.filter(task_id = int(idd))[0]
		if task.stop:
			return 'pass'
		else:
			task.status = 'runing'
			task.recent = False
			task.save()
		data = json.loads(nm_resp.content.decode())
		d_lst = data['InmateLocator']
		print(len(d_lst))
		for x in d_lst:
			print(x)

			task = Working_threads.objects.filter(task_id = int(idd))[0]
			if task.stop:
				break
			else:
				task.status = 'runing'
				task.save()

			try:
				if x['projRelDate'] == '':
					continue
				else:
					splited = x['projRelDate'].split('/')
					now = datetime.now()
					if int(splited[-1]) < now.year or int(splited[0]) < now.month:
						continue
				inmateNumber = x['inmateNum']
				inmateNumber = x['inmateNum']
				lst_inm = Inmate.objects.filter(inmate_num = inmateNumber)
				if len(lst_inm) == 0:
					pass
				else:
					continue



				json_resp = req.get(first_query.format(inmateNumber),headers=headers,proxies=sess_proxy)
				data = json.loads(json_resp.content.decode())
				data_list = data['InmateLocator']
				if len(data_list) == 0:
					input('data_list is null')
				else:
					dt_set = data_list[0]
					addr_resp = req.get(second_query.format(dt_set['faclCode']),headers=headers,proxies = sess_proxy)
					print('here')
					addr_data = json.loads(addr_resp.content.decode())
					addr_dict = addr_data['Addresses'][0]


					inmate = Inmate.objects.create(
						first_name = dt_set['nameFirst'],
						middle_name = dt_set['nameMiddle'],
						last_name = dt_set['nameLast'],
						sex = dt_set['sex'],
						race = dt_set['race'],
						age = int(dt_set['age']),
						inmate_num = dt_set['inmateNum'],
						facl_code = dt_set['faclCode'],
						relDate = dt_set['projRelDate'],
						region = addr_dict['region'],
						street = addr_dict['street'] + ' ' + addr_dict['street2'],
						city = addr_dict['city'],
						country = addr_dict['county'],
						state = addr_dict['state'],
						zipcode = addr_dict['zipCode'],
						faclDescription = addr_dict['faclTypeDescription'],
						areaCode  = addr_dict['areaCode']
					)
					inmate.save()

			except Exception as e:
				print(str(e))
				pass
		return 'pass'
	except Exception as e:
		return 'pass'



def import_dt(reader,facs):
	for row in reader:
		for line in facs:
			splited = line.split(',')
			if splited[2] in row[-1]:
				name_split = row[0].split(' ')
				if len(name_split) > 1:
					first_name = name_split[0]
					middle_name = name_split[1]
				else:
					first_name = name_split[0]
					middle_name = ''

				inmate = Inmate.objects.create(
						first_name = first_name,
						middle_name = middle_name,
						last_name = row[1],
						sex = row[5],
						race = row[4],
						age = row[3],
						inmate_num = row[2],
						facl_code = splited[1],
						relDate = row[6],
						region = '',
						street = splited[4],
						city = splited[5],
						country = '',
						state = splited[6],
						zipcode = splited[-1].split('\n')[0],
						faclDescription = splited[3],
						areaCode  = ''
					)
				print('saving')
				inmate.save()

def check_req(url,headers,sess_proxy,limit):
	counter = 0
	while counter < limit:
		try:
			resp = req.get(url,headers=headers,proxies=sess_proxy)
			return resp
		except:
			time.sleep(1)
			continue
	return False

def get_address_data(second_query,code,sess_proxy,headers):
	addr_resp = check_req(second_query.format(code),headers,sess_proxy,3)
	print('here')
	addr_data = json.loads(addr_resp.content.decode())
	addr_dict = addr_data['Addresses'][0]
	return addr_dict

def verify_location(idd,num,proxy):
	try:
		headers = {
	'user-agent' : 'Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0',}
		first_query = 'https://www.bop.gov/PublicInfo/execute/inmateloc?todo=query&output=json&inmateNum={0}&inmateNumType=IRN'
		second_query = 'https://www.bop.gov/PublicInfo/execute/phyloc?todo=query&output=json&code={0}'
		#sess_proxy = {'http': 'socks5h://'+proxy,'https': 'socks5h://'+proxy}
		sess_proxy = {'http':'http://'+proxy,'https':'https://'+proxy}
		print('here')
		task = verify_threads.objects.filter(task_id = int(idd))[0]
		if task.stop:
			return 'pass'
		else:
			task.recent = False
			task.status = 'runing'
			task.save()

		json_resp = check_req(first_query.format(num),headers,sess_proxy,3)
		data = json.loads(json_resp.content.decode())
		data_list = data['InmateLocator']
		if len(data_list) == 0:
			return "pass"
		else:
			dt = data_list[0]
			n = Inmate.objects.filter(inmate_num = num)[0]
			if n.facl_code == dt['faclCode']:
				return "pass"
			else:

				addr_dict = get_address_data(second_query,dt['faclCode'],sess_proxy,headers)
				n.region = addr_dict['region']
				n.street = addr_dict['street'] + ' ' + addr_dict['street2']
				n.city = addr_dict['city']
				n.country = addr_dict['county']
				n.state = addr_dict['state']
				n.zipcode = addr_dict['zipCode']
				n.faclDescription = addr_dict['faclTypeDescription']
				n.areaCode  = addr_dict['areaCode']
				n.verified = True
				n.save()
				print('modified')
			n.verified = True





	except Exception as e:
		print(str(e))
		return "pass"



def import_list(request):
	file_1 = open('new.txt','r')
	file_2 = open('data_list.csv','r')
	reader = csv.reader(file_2)
	facs = file_1.readlines()
	br = threading.Thread(target=import_dt,args=(reader,facs))
	br.daemon = True
	br.start()
	return HttpResponse('runing now')





def renew_proxies():
	try:
		resp = req.get('http://proxylist.online/proxy.php?key=8P4qLSH4ZTXz162bahoXA4hxJ7VXPos0')
		proxies = resp.content.decode().replace('\n','').split('\r')[:-1]

		return proxies
	except Exception as e:
		print('proxy_handler: Error renewing_proxies')


def get_starting_data(url,st,obj):
	time.sleep(3)
	resp = req.get(url)
	soup = BeautifulSoup(resp.content.decode(),'html.parser')
	table = soup.find('table',{'id':'myTable'})
	trs = table.findAll('tr')
	regex = re.compile('[^a-zA-Z]')
	for  x in trs[1:]:
		if st == 'name':
			name = Names.objects.create(name=regex.sub('',x.findAll('td')[0].text))
			name.save()
		else:
			surname = Surnames.objects.create(surname=regex.sub('',x.findAll('td')[0].text))
			surname.save()
	print('done this')
	obj.done = True
	obj.save()

def names_threads_update(*args):
	obj = Renewer.objects.create()
	obj.save()
	print(args)
	args_list = list(args)
	args_list.append(obj)
	print(args_list)
	bt = threading.Thread(target=get_starting_data,args=(x for x in args_list))
	bt.daemon = True
	bt.start()
	return True


def update_names(request):
	try:
		names_threads_update('https://names.mongabay.com/male_names_alpha.htm','name')
		names_threads_update('https://names.mongabay.com/female_names_alpha.htm','name')
		names_threads_update('https://names.mongabay.com/data/white.html','surname')
		final_status = names_threads_update('https://names.mongabay.com/data/black.html','surname')
		if final_status:
			return HttpResponse(json.dumps({
				'status':'success'
			}))
		else:
			return HttpResponse(json.dumps({
			'status':'failed'
		}))
	except Exception as e:
		print(str(e))
		return HttpResponse(json.dumps({
			'status':'failed'
		}))

def generate_combinations(names,surnames):
	try:
		for x in names:
			user = Names.objects.filter(name=x)
			user = user[0]
			user.used = True
			user.save()
			for y in surnames:
				yield (x,y)
	except Exception as e:
		raise e


# the starting function for the threads
def update(names,idd,proxy):
	global stop
	global message

	print('here')
	tasks = Working_threads.objects.filter(task_id=int(idd),status='stopped')
	if len(tasks) == 0:
		print('here')
		task = Working_threads.objects.create(task_id = int(idd),status='stopped',stop=False)
		task.save()
		print('saved')
	else:
		task = tasks[0]
		task.status = 'stopped'
		task.stop = False
		task.save()

	id_list.append(int(idd))

	while True:
		names = names
		surnames = [x.surname for x in Surnames.objects.all() ]
		for comb in generate_combinations(names,surnames):
			task = Working_threads.objects.filter(task_id = int(idd))[0]

			if task.stop:
				task.status = 'stopped'
				task.save()
				break
			else:
				get_inmate_data(comb[0],comb[1],idd,proxy)
		if task.stop:
			task.status = 'stopped'
			task.save()
			break
def update_verify(inmates,idd,proxy):
	global stop
	global message

	tasks = verify_threads.objects.filter(task_id=int(idd),status='stopped')
	if len(tasks) == 0:
		task = verify_threads.objects.create(task_id = int(idd),status='stopped',stop=False)
		task.save()
	else:
		task = tasks[0]
		task.status = 'stopped'
		task.stop = False
		task.save()
	verify_id_list.append(int(idd))
	while True:
		time.sleep(0.3)
		for  inm in inmates:
			task = verify_threads.objects.filter(task_id = int(idd))[0]

			if task.stop:
				task.status = 'stopped'
				task.save()
				break
			else:
				print(inm.inmate_num)
				verify_location(idd,inm.inmate_num,proxy)
		if task.stop:
			task.status = 'stopped'
			task.save()
			break


#[x.name for x in Names.objects.filter(used=False)]

def reset_names(request):
	nms = Names.objects.filter(used=True)
	for nm in nms:
		nm.used = False
		nm.save()



def reset_verify(request):
	nms = Inmate.objects.filter(verified=True)
	for nm in nms:
		nm.used = False
		nm.save()
	return HttpResponse('success')

def reset_internally_verify():
	nms = Inmate.objects.filter(verified=True)
	for nm in nms:
		nm.used = False
		nm.save()
def reset_internally():
	nms = Names.objects.filter(used=True)
	for nm in nms:
		nm.used = False
		nm.save()


def generate_threads(count):
	try:
		#proxies = renew_proxies()

		fnames = Names.objects.filter(used=False)
		not_list = [x.done for x in Renewer.objects.all()]
		if all(not_list) and len(fnames) != 0:
			pass
		else:
			fnames_test = Names.objects.all()
			if len(fnames_test) != 0:
				return [False,'renewers not done yet']
			else:
				reset_internally()
				return [False,'rerun in a moment']
		rate = len(fnames) // count
		print(rate)
		for y in range(count):
			if rate == len(fnames):
				print('here')
				partial_list = fnames
			elif y == (count-1):
				partial_list = fnames[rate*y:]
			else:
				partial_list = fnames[rate*y:rate*(y+1)]
			proxy = '108.59.14.200:13152'
			#proxies.remove(proxy)
			bt = threading.Thread(target=update,args=(partial_list,str(y),proxy))
			bt.daemon = True
			bt.start()
		return [True,str(y)]
	except Exception as e:
		raise e

def generate_threads_verify(count):
	try:
		#proxies = renew_proxies()

		fnames = Inmate.objects.filter(verified=False)

		if  len(fnames) != 0:
			pass
		else:
			fnames_test = Inmate.objects.all()
			if len(fnames_test) == 0:
				return [False,'no inmates yet']
			else:
				reset_internally_verify()
				return [False,'rerun in a moment']
		rate = len(fnames) // count
		print(rate)
		for y in range(count):
			if rate == len(fnames):
				print('here')
				partial_list = fnames
			elif y == (count-1):
				partial_list = fnames[rate*y:]
			else:
				partial_list = fnames[rate*y:rate*(y+1)]
			proxy = '108.59.14.200:13152'
			#proxies.remove(proxy)
			bt = threading.Thread(target=update_verify,args=(partial_list,str(y),proxy))
			bt.daemon = True
			bt.start()
		return [True,str(y)]
	except Exception as e:
		raise e






# thread stop

# views code
def start_verifier(request):
	try:
		count = request.GET.get('count','1')
		stop_second()
		resp = generate_threads_verify(int(count))
		#resp = [True,'this test']
		print(resp)
		if resp[0]:
			return HttpResponse(json.dumps({
				'status':'runing',
				'message':resp[-1]
			}))
		else:
			return HttpResponse(json.dumps({
			'status':'failed',
			'message':resp[-1]
		}))

	except Exception as e:
		print(str(e))
		return HttpResponse(json.dumps({
			'status':'failed'
		}))

def start(request):
	try:
		count = request.GET.get('count','1')
		stop_first()

		resp = generate_threads(int(count))

		print(resp)
		if resp[0]:
			return HttpResponse(json.dumps({
				'status':'runing',
				'message':resp[-1]
			}))
		else:
			return HttpResponse(json.dumps({
			'status':'failed',
			'message':resp[-1]
		}))

	except Exception as e:
		print(str(e))
		return HttpResponse(json.dumps({
			'status':'failed'
		}))
def stop_first():
	tasks = Working_threads.objects.all()
	for x in tasks:
		x.delete()



def stop_second():
	tasks = verify_threads.objects.all()
	for x in tasks:
		x.delete()






def stop_thread(request):

	idd = request.GET.get('idd','')
	count = request.GET.get('count','')
	if idd != '':
		id_list.remove(idd)
		task = Working_threads.objects.filter(task_id = int(idd),status = 'runing')[0]
		task.stop = True
		task.save()
		return HttpResponse(json.dumps({
			'status':'stoped'
		}))
	else:
		if len(id_list) != 0:
			if count != '':
				count = int(count)
				print(id_list)
				if count <= len(id_list):
					counter = 0
					while counter < count:
						idd = id_list[0]
						id_list.remove(idd)
						task = Working_threads.objects.filter(task_id = int(idd),status = 'runing')[0]
						task.stop = True
						task.save()
						counter += 1
					return HttpResponse(json.dumps({
								'status':'stoped',
								'length':str(counter)
							}))
				else:
					for idd in id_list:
						id_list.remove(idd)
						task = Working_threads.objects.filter(task_id = int(idd),status = 'runing')[0]
						task.stop = True
						task.save()
					return HttpResponse(json.dumps({
								'status':'stoped',
								'length':str(count)
							}))





			else:

				idd = id_list[0]
				id_list.remove(idd)
				task = Working_threads.objects.filter(task_id = int(idd),status = 'runing')[0]
				task.stop = True
				task.save()
				return HttpResponse(json.dumps({
				'status':'stoped',
				'idd' : str(idd)
			}))
		else:
			pass


			return HttpResponse(json.dumps({
				'status':'stoped None'
			}))


def stop_thread_verify(request):

	idd = request.GET.get('idd','')
	count = request.GET.get('count','')
	if idd != '':
		verify_id_list.remove(idd)
		task = verify_threads.objects.filter(task_id = int(idd),status = 'runing')[0]
		task.stop = True
		task.save()
		return HttpResponse(json.dumps({
			'status':'stoped'
		}))
	else:
		print(verify_id_list)
		if len(verify_id_list) != 0:
			if count != '':
				count = int(count)
				if count <= len(verify_id_list):
					counter = 0
					while counter < count:
						print('stoping')
						idd = verify_id_list[0]
						verify_id_list.remove(idd)
						task = verify_threads.objects.filter(task_id = int(idd),status = 'runing')[0]
						task.stop = True
						task.save()
						counter += 1
					return HttpResponse(json.dumps({
							'status':'stoped',
							'length':str(counter)
						}))

				else:
					for idd in verify_id_list:
						print('stoping')
						print(idd)
						verify_id_list.remove(idd)
						task = verify_threads.objects.filter(task_id = int(idd),status = 'runing')[0]
						task.stop = True
						task.save()

					return HttpResponse(json.dumps({
							'status':'stoped',
							'length':str(count)
						}))



			else:

				idd = verify_id_list[0]
				verify_id_list.remove(idd)
				task = verify_threads.objects.filter(task_id = int(idd),status = 'runing')[0]
				task.stop = True
				task.save()
				return HttpResponse(json.dumps({
			'status':'stoped',
			'idd' : str(idd)
		}))
		else:
			pass


			return HttpResponse(json.dumps({
				'status':'stoped None'
			}))
def g_threads():
	if len(id_list) == 0:
		return {}
	else:
		ret_dict = {}
		for x in id_list:
			task = Working_threads.objects.filter(task_id = int(x))[0]
			if 'stopped' in task.status and not task.recent:
				task.delete()
			else:
				ret_dict[str(task.task_id)] = task.status
	return ret_dict
def get_threads(request):
	ret_dict = g_threads()
	return HttpResponse(json.dumps({
		'len':str(len(threading.enumerate())),
		'message':ret_dict
	}))
#delete stoped threads
def g_threads_verify():
	if len(verify_id_list) == 0:
		return {}
	else:
		ret_dict = {}
		for x in verify_id_list:
			task = verify_threads.objects.filter(task_id = int(x))[0]
			if 'stopped' in task.status and not task.recent:
				task.delete()
			else:
				ret_dict[str(task.task_id)] = task.status
	return ret_dict

# get working threads
def get_threads_verify(request):
	ret_dict = g_threads_verify()
	return HttpResponse(json.dumps({
		'len':str(len(threading.enumerate())),
		'message':ret_dict
	}))



# returning inmate data
def format_resp_inmate(user):

	ret_dict = {
		'first_name':user.first_name,
		'middle_name':user.middle_name,
		'last_name' :user.last_name,
		'sex' : user.sex,
		'race' : user.race,
		'age' : user.age,
		'inmate_num' : user.inmate_num,
		'facl_code' : user.facl_code,
		'rel_date' : user.relDate,
		'region': user.region,
		'street' : user.street,
		'city': user.city,
		'county' : user.country,
		'state' :user.state,
		'zipcode' : user.zipcode,
		'faclDescription' : user.faclDescription,
		'areaCode' : user.areaCode
	}

	return ret_dict


#fitlering view
def filter_dt(request):
	if request.method == 'GET':
		name = request.GET.get('name','').upper()
		name_start = request.GET.get('nameStart','').upper()
		surname = request.GET.get('surname','').upper()
		surname_start = request.GET.get('surnameStart','').upper()
		age = request.GET.get('age','')
		date_start = request.GET.get('dateStart','')
		date_end = request.GET.get('dateEnd','end')
		race = request.GET.get('race','')
		if '-' in age:
			age_sp = age.split('-')
			age_st = age_sp[0]
			age_end = age_sp[-1]
			age = ''
		else:
			age_st = ''
			age_end = ''
		if name != '':
			if surname != '':
				if age != '':
					users = Inmate.objects.filter(filtered=False,first_name=name,last_name = surname,age=int(age))
					if len(users) != 0:
						ret_list = []
						for user in users:
							ret_list.append(format_resp_inmate(user))
					else:
						return HttpResponse(json.dumps({
							'status':'fail',
							'response':'no inmates found'
						}))
				else:
					users = Inmate.objects.filter(filtered=False,first_name=name,last_name = surname)
					if len(users) != 0:
						ret_list = []
						for user in users:
							ret_list.append(format_resp_inmate(user))
					else:
						return HttpResponse(json.dumps({
							'status':'fail',
							'response':'no inmates found'
						}))
			else:
				if surname_start != '':
					if age != '':
						users = Inmate.objects.filter(filtered=False,first_name=name,last_name__startswith = surname_start,age=int(age))
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
					else:
						users = Inmate.objects.filter(filtered=False,first_name=name,last_name__startswith = surname_start)
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
				else:
					if age != '':
						users = Inmate.objects.filter(filtered=False,first_name=name,age=int(age))
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
					else:
						users = Inmate.objects.filter(filtered=False,first_name=name)
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
		else:
			if name_start != '':
				if surname != '':
					if age != '':
						users = Inmate.objects.filter(filtered=False,first_name__startswith=name_start,last_name = surname,age=int(age))
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
					else:
						users = Inmate.objects.filter(filtered=False,first_name__startswith=name_start,last_name = surname)
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
				else:
					if surname_start != '':
						if age != '':
							users = Inmate.objects.filter(filtered=False,first_name__startswith=name_start,last_name__startswith = surname_start,age=int(age))
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
						else:
							users = Inmate.objects.filter(filtered=False,first_name__startswith=name_start,last_name__startswith = surname_start)
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
					else:
						if age != '':
							users = Inmate.objects.filter(filtered=False,first_name__startswith=name_start,age=int(age))
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
						else:
							users = Inmate.objects.filter(filtered=False,first_name__startswith=name_start)
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
			else:
				if surname != '':
					if age != '':
						users = Inmate.objects.filter(filtered=False,last_name = surname,age=int(age))
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
					else:
						users = Inmate.objects.filter(filtered=False,last_name = surname)
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
				else:
					if surname_start != '':
						if age != '':
							users = Inmate.objects.filter(filtered=False,last_name__startswith = surname_start,age=int(age))
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
						else:
							users = Inmate.objects.filter(filtered=False,last_name__startswith = surname_start)
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
					else:
						if age != '':
							users = Inmate.objects.filter(filtered=False,age=int(age))
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
						else:
							users = Inmate.objects.filter(filtered=False)
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
		if race != '':
			ret_list = filter_race(race,ret_list)

		if age_st != '' and age_end != '':
			ret_list = filter_age(age_st,age_end,ret_list)
		else:
			pass

		if date_start != '' and date_end != '':
			final_list = filter_list(date_start,date_end,ret_list)
		else:
			final_list = ret_list
		used_list(final_list)
		return HttpResponse(json.dumps({
			'status':'success',
			'response':final_list
		}))
	else:
		return HttpResponse('Use get requests')

def used_list(lst):
	for  x in lst:
		inm = Inmate.objects.filter(inmate_num = x['inmate_num'])[0]
		inm.filtered = True
		inm.save()




def filter_ui(request):
	if request.method == 'GET':
		name = request.GET.get('name','').upper()
		name_start = request.GET.get('nameStart','').upper()
		surname = request.GET.get('surname','').upper()
		surname_start = request.GET.get('surnameStart','').upper()
		age = request.GET.get('age','')
		date_start = request.GET.get('dateStart','')
		print(date_start)
		date_end = request.GET.get('dateEnd','')
		print(date_end)
		white = request.GET.get('white','')
		if 'on' in white:
			white = 'white'.lower()
		black = request.GET.get('black','')
		if 'on' in black:
			black = 'black'.lower()
		asian = request.GET.get('asian','')
		if 'on' in asian:
			asian = 'asian'.lower()
		indian = request.GET.get('indian','')
		if 'on' in indian:
			indian = 'American Indian'.lower()
		race = [white,black,asian,indian]
		count = request.GET.get('count','')
		fac_code = request.GET.get('facility','')
		if '-' in age:
			age_sp = age.split('-')
			age_st = age_sp[0]
			age_end = age_sp[-1]
			age = ''
		else:
			age_st = ''
			age_end = ''
		if name != '':
			if surname != '':
				if age != '':
					users = Inmate.objects.filter(filtered=False,first_name=name,last_name = surname,age=int(age))
					if len(users) != 0:
						ret_list = []
						for user in users:
							ret_list.append(format_resp_inmate(user))
					else:
						return HttpResponse(json.dumps({
							'status':'fail',
							'response':'no inmates found'
						}))
				else:
					users = Inmate.objects.filter(filtered=False,first_name=name,last_name = surname)
					if len(users) != 0:
						ret_list = []
						for user in users:
							ret_list.append(format_resp_inmate(user))
					else:
						return HttpResponse(json.dumps({
							'status':'fail',
							'response':'no inmates found'
						}))
			else:
				if surname_start != '':
					if age != '':
						users = Inmate.objects.filter(filtered=False,first_name=name,last_name__startswith = surname_start,age=int(age))
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
					else:
						users = Inmate.objects.filter(filtered=False,first_name=name,last_name__startswith = surname_start)
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
				else:
					if age != '':
						users = Inmate.objects.filter(filtered=False,first_name=name,age=int(age))
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
					else:
						users = Inmate.objects.filter(filtered=False,first_name=name)
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
		else:
			if name_start != '':
				if surname != '':
					if age != '':
						users = Inmate.objects.filter(filtered=False,first_name__startswith=name_start,last_name = surname,age=int(age))
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
					else:
						users = Inmate.objects.filter(filtered=False,first_name__startswith=name_start,last_name = surname)
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
				else:
					if surname_start != '':
						if age != '':
							users = Inmate.objects.filter(filtered=False,first_name__startswith=name_start,last_name__startswith = surname_start,age=int(age))
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
						else:
							users = Inmate.objects.filter(filtered=False,first_name__startswith=name_start,last_name__startswith = surname_start)
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
					else:
						if age != '':
							users = Inmate.objects.filter(filtered=False,first_name__startswith=name_start,age=int(age))
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
						else:
							users = Inmate.objects.filter(filtered=False,first_name__startswith=name_start)
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
			else:
				if surname != '':
					if age != '':
						users = Inmate.objects.filter(filtered=False,last_name = surname,age=int(age))
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
					else:
						users = Inmate.objects.filter(filtered=False,last_name = surname)
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
				else:
					if surname_start != '':
						if age != '':
							users = Inmate.objects.filter(filtered=False,last_name__startswith = surname_start,age=int(age))
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
						else:
							users = Inmate.objects.filter(filtered=False,last_name__startswith = surname_start)
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
					else:
						if age != '':
							users = Inmate.objects.filter(filtered=False,age=int(age))
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
						else:
							users = Inmate.objects.filter(filtered=False)
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
		print(race)
		if any(check_list(race)) :
			print('doing')
			ret_list = filter_race(race,ret_list)

		if age_st != '' and age_end != '':
			ret_list = filter_age(age_st,age_end,ret_list)
		else:
			pass

		if fac_code != '':
			ret_list = filter_fac(fac_code,ret_list)
		else:
			pass

		if date_start != '' and date_end != '':
			final_list = filter_list(date_start,date_end,ret_list)
		else:
			final_list = ret_list

		print(len(final_list))

		if count != '':
			count = int(count)
			if len(final_list) <= count:
				pass
			else:
				div_count = count - (count%30)
				if div_count == 0:
					pass
				else:
					final_list = final_list[:div_count]
		else:
			pass

		used_list(final_list)
		resp = format_pdf(final_list)
		return resp
	else:
		return HttpResponse('Use get requests')

def check_list(lst):
	resp = []
	for x in lst:
		if '' == x:
			resp.append(False)
		else:
			resp.append(True)
	return resp

def filter_fac(code,lst):
	ret_list = []
	for inm in lst:
		if  code == inm['facl_code']:
			ret_list.append(inm)
		else:
			pass
	return ret_list


def filter_race(race,lst):
	ret_list = []
	for inm in lst:
		if inm['race'].lower().strip() in race:
			ret_list.append(inm)
		else:
			pass
	return ret_list


def filter_age(st,end,lst):
	ret_list  = []
	for inm in lst:
		age = int(inm['age'])
		if  int(st) <= age and int(end) >= age:
			ret_list.append(inm)
		else:
			pass

	return ret_list

def filter_list(st,end,lst):
	if '/' not in st and '/' not in end:
		return False
	final_list = []
	for inm in lst:
		try:

			date_splited = [int(x) for x in list(inm['rel_date'].split('/'))]
			st_splited = [int(x) for x in list(st.split('/'))]
			end_splited = [int(x) for x in list(end.split('/'))]
			if date_splited[-1] >= st_splited[-1] and date_splited[-1] <= end_splited[-1]:
				if date_splited[0] >= st_splited[0] and date_splited[0]  <= end_splited[0]:
					if date_splited[-2]  >= st_splited[-2] and date_splited[-2] <= end_splited[-2]:
						final_list.append(inm)
					else:
						continue
				else:
					continue
			else:
				continue
		except:
			continue
	return final_list


specs = labels.Specification(215.9, 279.4, 3, 10, 66.68, 25.4, corner_radius=2)

main_string = """LAMAR JAMES FORD\n09062041\nFEDERAL PRISON CAMP\nP.O. BOX 1000 DULUTH, MN  55814\n"""
# Create a function to draw each label. This will be given the ReportLab drawing
# object to draw on, the dimensions (NB. these will be in points, the unit
# ReportLab uses) of the label, and the object to render.
def draw_label(label, width, height, obj):
    # Just convert the object to a string and print this at the bottom left of
    # the label.
	splited = str(obj).split('\n')
	starting_range = 50

	counter = 50
	for st in splited:

		if counter == starting_range:
			label.add(shapes.String(4, counter, st.strip(), fontName="Helvetica", fontSize=15))
		else:
			label.add(shapes.String(4, counter, st.strip(), fontName="Helvetica", fontSize=10))
		counter -= 15
		if counter <= 0:
			break
		else:
			continue

def format_message(dct):
	message = dct['first_name'] + ' ' + dct['middle_name'] + ' ' + dct['last_name']  + '\n'+ dct['inmate_num'] + '\n' + dct['faclDescription'] + '\n' + dct['street'] + ' ' + dct['city'] + ' ' +  dct['state'] + ' ' + dct['zipcode']
	return message


def crt(filename,lst):
	# Create the sheet.
	sheet = labels.Sheet(specs, draw_label, border=False)


	for i in lst:
		main_string = format_message(i)
		sheet.add_label(main_string)


	sheet.save(filename)


def generate_pdf(request):
	if request.method == 'GET':
		inmateNum = request.GET.get('inmateNum','')
		if inmateNum != '':
			buffer = io.BytesIO()
			canva = canvas.Canvas(buffer)
			canva.drawString(100, 100, "Hello world.")
			canva.showPage()
			canva.save()
			return FileResponse(buffer,as_attachment=True,filename='hello.pdf')
		else:
			return HttpResponse(json.dumps({
				'status':'failed',
				'message':'provide a inmateNum'
			}))
	else:
		return HttpResponse(json.dumps({
				'status':'failed',
				'message':'Use a get request'
			}))

			#['Black', 'White', 'American Indian', 'Asian']
def format_pdf(final_list):
	filename = str(random.randrange(1000,9999999))+'.pdf'
	crt(filename,final_list)
	file_t = open(filename,'rb')
	buffer = file_t.read()
	file_t.close()
	os.remove(filename)
	response = HttpResponse(buffer, content_type='application/pdf')
	response['Content-Disposition'] = 'attachment; filename="' + filename + '"'
	return response

def reset(request):
	inmates = Inmate.objects.filter(filtered=True)
	for inm in inmates:
		inm.filtered = False
		inm.save()

	return HttpResponse(json.dumps({
		'status':'success'
	}))

def print_page(request):
	facilities = Facilities.objects.all()
	return render(request,'filterer.html',{
		'facilities':facilities
	})


def get_details_update(request):
	names_dict = g_threads()
	inmates_dict = g_threads_verify()
	current = Tracker.objects.all()[0]
	inmates = Inmate.objects.filter(verified = True)

	return HttpResponse(json.dumps({
		'names_proc':str(len(names_dict.keys())),
		'inmates_proc':str(len(inmates_dict.keys())),
		'current_number':str(current.current_number),
		'processed_inmates':str(len(inmates))

	}))
def control_pannel(request):
	return render(request,'pannel.html')
