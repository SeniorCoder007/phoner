from django.db import models
from django.utils.timezone import now
import datetime

# Create your models here.


class Inmate(models.Model):
	first_name = models.CharField(max_length=255)
	middle_name = models.CharField(max_length=255)
	last_name = models.CharField(max_length=255)
	sex = models.CharField(max_length=255)
	race = models.CharField(max_length=255)
	age = models.IntegerField(default=0)
	inmate_num = models.CharField(max_length=255,unique=False)
	facl_code = models.CharField(max_length=255)
	relDate = models.CharField(max_length=255)
	region =models.CharField(max_length=255)
	street = models.CharField(max_length=255)
	city = models.CharField(max_length=255)
	country = models.CharField(max_length=255)
	state = models.CharField(max_length=255)
	zipcode = models.CharField(max_length=255)
	faclDescription = models.CharField(max_length=255)
	areaCode = models.CharField(max_length=255)
	filtered  = models.BooleanField(default=False)
	printed = models.BooleanField(default=False)
	verified = models.BooleanField(default=False)
	lastprinted = models.DateTimeField(default=now)
	lastverified = models.DateTimeField(default=now)
	created = models.DateTimeField(default=now)

	def __str__(self):
		return self.first_name + ' ' + self.middle_name + ' ' + self.last_name

class Facilities(models.Model):
	code = models.CharField(max_length=255)
	description = models.CharField(max_length=255)
	address = models.CharField(max_length=1000,default='')

	def __str__(self):
		return self.code + ' : ' + self.description
class Names(models.Model):
	name = models.CharField(max_length=255)
	used = models.BooleanField(default=False)

	def __str__(self):
		return self.name

class Surnames(models.Model):
	surname = models.CharField(max_length=255)
	used = models.BooleanField(default=False)


	def __str__(self):
		return self.surname

class Renewer(models.Model):
	done = models.BooleanField(default=False)

	def __str__(self):
		return str(self.done)

class Working_threads(models.Model):
	task_id = models.IntegerField(default=0,unique=True)
	status = models.CharField(max_length=255)
	stop = models.BooleanField(default=False)
	recent = models.BooleanField(default=True)

	def __str__(self):
		return str(self.task_id) + ' ' + self.status

class verify_threads(models.Model):
	task_id = models.IntegerField(default=0,unique=True)
	status = models.CharField(max_length=255)
	stop = models.BooleanField(default=False)
	recent = models.BooleanField(default=True)

	def __str__(self):
		return str(self.task_id) + ' ' + self.status

class Tracker(models.Model):
	current_number = models.CharField(max_length=255,default='00000-000')

	def __str__(self):
		return str(self.current_number)

class custInm(models.Model):
	cust_id = models.CharField(max_length=250)
	inmates = models.TextField(default='')
	out_c = models.CharField(max_length=20)
	done = models.BooleanField(default=False)

	def __str__(self):
		return self.cust_id


class Proxies(models.Model):
	host = models.CharField(max_length=500)
	port = models.IntegerField()
	idd = models.IntegerField(default=0)
	def __str__(self):
		return f'{self.host}:{str(self.port)}'

class ProxySet(models.Model):
	proxy = models.IntegerField(default=1)

	def __str__(self):
		return str(self.proxy)

class ViewProtection(models.Model):
	password = models.CharField(max_length=1000,default='21Corrlinks$')

	def __str__(self):
		return self.password
