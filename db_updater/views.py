from django.shortcuts import render,HttpResponse,redirect
from django.utils import timezone
from .models import *
import json
import os
import threading
import time
from datetime import datetime
import requests as req
import random
from bs4 import BeautifulSoup
import re
from django.http import FileResponse
from reportlab.pdfgen import canvas
import io
import labels
from reportlab.graphics import shapes
import csv
from io import BytesIO
import zipfile
import datetime
import random


username = 'lum-customer-hl_2c924221-zone-zone1'
password = 'idrx4w6pqq1s'
port = 22225
id_list = []

verify_id_list = []

endings = ['000', '001', '002', '003', '004', '006', '007', '008', '009', '010', '011', '012', '013', '014', '015', '016', '017', '018', '019', '020', '021', '022', '023', '024', '025', '026', '027', '028', '029', '030', '031', '032', '033', '034', '035', '036', '037', '038', '039', '040', '041', '042', '043', '044', '045', '046', '047', '048', '049', '050', '051', '052', '053', '054', '055', '056', '057', '058', '059', '060', '061', '062', '063', '064', '065', '066', '067', '068', '069', '070', '071', '073', '074', '075', '076', '077', '078', '079', '080', '081', '082', '083', '084', '085', '086', '087', '088', '089', '090', '091', '093', '094', '095', '097', '098', '099', '101', '103', '104', '107', '111', '112', '115', '116', '117', '118', '120', '122', '123', '128', '131', '132', '133', '135', '136', '138', '145', '146', '148', '149', '151', '156', '158', '160', '163', '164', '171', '173', '175', '177', '178', '179', '180', '183', '184', '190', '196', '198', '208', '265', '273', '279', '280', '284', '287', '298', '308', '359', '379', '380', '408', '424', '479', '480', '508', '522', '555', '707', '748', '789', '834', '899']

#02739-081


def run_req(url,type='get',params=None,proxy=None):
	headers = {
'user-agent' : 'Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0',
}
	print(proxy)
	while True:
		try:
			if type=='get':
				if proxy:
					resp = req.get(url,headers=headers ,proxies=proxy)
					return resp
				else:
					resp = req.get(url,headers=headers) #,proxies=sess_proxy)
					return resp
			else:
				if proxy:
					if params:
						resp = req.post(url,headers=headers,data=params,proxies=proxy)
						return resp
					else:
						resp = req.post(url,headers=headers,proxies=proxy)
						return resp
				else:
					if params:
						resp = req.post(url,headers=headers,data=params)
						return resp
					else:
						resp = req.post(url,headers=headers)
						return resp
		except Exception as e:
			print(str(e))
			time.sleep(5)
			continue

def get_inmate_data(inmnum,idd,proxy):
	try:
		global stop
		global message
		headers = {
	'user-agent' : 'Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0',
}
		session_id = random.random()
		sess_proxy = {'http': 'socks5h://'+proxy,'https': 'socks5h://'+proxy}
		sess_proxy = {'http':'http://'+proxy,'https':'https://'+proxy}
		main_query  = 'https://www.bop.gov/PublicInfo/execute/inmateloc?todo=query&output=json&inmateNum=&nameFirst={0}&nameMiddle=&nameLast={1}&age=&race=&sex='
		first_query = 'https://www.bop.gov/PublicInfo/execute/inmateloc?todo=query&output=json&inmateNum={0}&inmateNumType=IRN'
		second_query = 'https://www.bop.gov/PublicInfo/execute/phyloc?todo=query&output=json&code={0}'
		#nm_resp = req.get(main_query.format(name,surname),headers=headers,proxies = sess_proxy)
		task = Working_threads.objects.filter(task_id = int(idd))[0]
		if task.stop:
			return 'stop'
		else:
			task.status = 'runing'
			task.recent = False
			task.save()
		print('test')
		#data = json.loads(nm_resp.content.decode())
		#d_lst = data['InmateLocator']
		#print(len(d_lst))
		try:

			task = Working_threads.objects.filter(task_id = int(idd))[0]
			if task.stop:
				task.status = 'stoped'
				task.save()
				return 'stop'
			else:
				task.status = 'runing'
				task.save()


				inmateNumber = inmnum

				lst_inm = Inmate.objects.filter(inmate_num = inmateNumber)
				print(lst_inm)
				if len(lst_inm) == 0:
					pass
				else:
					if len(lst_inm) > 1:
						for x in lst_inm:
							x.delete()
					else:
						return 'pass'



				json_resp = run_req(first_query.format(inmateNumber),proxy=sess_proxy)
				data = json.loads(json_resp.content.decode())
				data_list = data['InmateLocator']
				if len(data_list) == 0:
					print('data_list is null')
					return "pass"
				else:
					dt_set = data_list[0]
					try:
						if dt_set['projRelDate'] == '':
							print('date')
							return 'pass'
						else:
							splited = dt_set['projRelDate'].split('/')
							now = datetime.datetime.now()
							if int(splited[-1]) > now.year:
								pass
							elif int(splited[-1]) == now.year :
								if int(splited[0]) > now.month:
									pass
								elif int(splited[0]) == now.month:
									if int(splited[1]) > now.day:
										pass
									else:
										return 'pass'

								else:
									return 'pass'

							else:
								return 'pass'
					except Exception as e:
						return 'pass'

					try:
						addr_resp = run_req(second_query.format(dt_set['faclCode']),proxy = sess_proxy)
						print('here')
						addr_data = json.loads(addr_resp.content.decode())
						addr_dict = addr_data['Addresses'][0]
					except:
						addr_dict = {
						'region': '',
						'street' : '',
						'street2' : '',
						'city': '',
						'county' : '',
						'state' :'',
						'zipCode' : '',
						'faclDescription' : '',
						'areaCode' : ''

						}

					print('saving')


					inmate = Inmate.objects.create(
						first_name = dt_set['nameFirst'],
						middle_name = dt_set['nameMiddle'],
						last_name = dt_set['nameLast'],
						sex = dt_set['sex'],
						race = dt_set['race'],
						age = int(dt_set['age']),
						inmate_num = dt_set['inmateNum'],
						facl_code = dt_set['faclCode'],
						relDate = dt_set['projRelDate'],
						region = addr_dict['region'],
						street = addr_dict['street'] + ' ' + addr_dict['street2'],
						city = addr_dict['city'],
						country = addr_dict['county'],
						state = addr_dict['state'],
						zipcode = addr_dict['zipCode'],
						faclDescription = addr_dict['faclTypeDescription'],
						areaCode  = addr_dict['areaCode']
					)
					inmate.save()


		except Exception as e:
			print(str(e))
			pass
		return 'pass'
	except Exception as e:
		print(str(e))
		return 'pass'

def get_address_data(second_query,code,sess_proxy):
	try:
		addr_resp = run_req(second_query.format(code),proxy=sess_proxy)
		print('here')
		addr_data = json.loads(addr_resp.content.decode())
		addr_dict = addr_data['Addresses'][0]
	except:
		addr_dict = {
		'region': '',
		'street' : '',
		'street2' : '',
		'city': '',
		'county' : '',
		'state' :'',
		'zipCode' : '',
		'faclDescription' : '',
		'areaCode' : ''

		}

	return addr_dict

def verify_location(idd,num,proxy):
	try:
		headers = {
	'user-agent' : 'Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0',}
		first_query = 'https://www.bop.gov/PublicInfo/execute/inmateloc?todo=query&output=json&inmateNum={0}&inmateNumType=IRN'
		second_query = 'https://www.bop.gov/PublicInfo/execute/phyloc?todo=query&output=json&code={0}'
		sess_proxy = {'http': 'socks5h://'+proxy,'https': 'socks5h://'+proxy}
		sess_proxy = {'http':'http://'+proxy,'https':'https://'+proxy}
		print('here')
		task = verify_threads.objects.filter(task_id = int(idd))[0]
		if task.stop:
			return 'pass'
		else:
			task.recent = False
			task.status = 'runing'
			task.save()

		json_resp = run_req(first_query.format(num),proxy = sess_proxy)
		data = json.loads(json_resp.content.decode())
		data_list = data['InmateLocator']
		if len(data_list) == 0:
			return "pass"
		else:
			dt = data_list[0]
			n = Inmate.objects.filter(inmate_num = num)
			if len(n) > 1:
				for x in n[1:]:
					x.delete()
				n = Inmate.objects.filter(inmate_num = num)[0]
			else:
				n = n[0]
			if dt['faclCode'] == '':
				#n.delete()
				pass
			if n.facl_code == dt['faclCode']:
				print('not changed')

			else:

				addr_dict = get_address_data(second_query,dt['faclCode'],sess_proxy)
				#input(addr_dict)
				n.facl_code = dt['faclCode']
				n.region = addr_dict['region']
				n.street = addr_dict['street'] + ' ' + addr_dict['street2']
				n.city = addr_dict['city']
				n.country = addr_dict['county']
				n.state = addr_dict['state']
				n.zipcode = addr_dict['zipCode']
				n.faclDescription = addr_dict['faclTypeDescription']
				n.areaCode  = addr_dict['areaCode']
				n.lastverified = timezone.now()
				n.verified = True

				print('modified')
			n.verified = True
			n.save()
			return "pass"





	except Exception as e:
		print(str(e))
		return "pass"


### for name guesser ####



def format(rate,num):
	length = len(str(num))
	if length == rate:
		return str(num)
	else:
		final_num = ''
		for _ in range(rate - length):
			final_num += '0'
		final_num += str(num)
		return final_num

def check_runing(idd):
	for x in threading.enumerate():
		if x.name == str(idd):
			return True
		else:
			return False




def update(idd,proxy):
	global stop
	global message

	print('here')
	tasks = Working_threads.objects.filter(task_id=int(idd))
	if len(tasks) == 0:
		print('here')
		task = Working_threads.objects.create(task_id = int(idd),status='stopped',stop=False)
		task.save()
		print('saved')
	else:
		task = tasks[0]
		if check_runing(task.task_id):
			pass
		else:
			task.status = 'stopped'
			task.stop = False
			task.save()

	id_list.append(int(idd))

	while True:

		current = Tracker.objects.all()[0]
		curr = current.current_number
		if curr == '99999-899':
			current.current_number = '00000-000'
			current.save()
			num = current.current_number
		else:
			splited = curr.split('-')
			index = endings.index(splited[-1])
			if index+1 == len(endings):
				part1 = format(5,int(splited[0]) + 1)
				num = part1 + '-' + '000'
			else:
				#part2 = format(3,int(splited[-1]) + 1)
				part2 = endings[index+1]
				num = splited[0] + '-' + part2
			print(num)
			current.current_number = num


		task = Working_threads.objects.filter(task_id = int(idd))[0]

		if task.stop:
			task.status = 'stopped'
			task.save()
			break
		else:
			current.save()
			#input('check')
			res = get_inmate_data(num,idd,proxy)
			if res == 'stop':
				pass
			else:
				pass



		task = Working_threads.objects.filter(task_id = int(idd))[0]

		if task.stop:
			task.status = 'stopped'
			task.save()
			break

def clear_threads(count):
	lst = [int(x) for x in range(count)]
	tasks  =  Working_threads.objects.all()
	for task in tasks:
		if task.task_id in lst:
			pass
		else:
			if check_runing(task.task_id):
				pass
			else:
				task.status = 'stopped'
			task.stop = True
			task.save()



def generate_threads(count):
	try:
		clear_threads(count)
		for y in range(count):

			#proxy = '108.59.14.200:13152'
			#proxy = '5.79.66.2:13200'
			s = ProxySet.objects.all()[0]
			#if s.proxy == 1:
			#	proxy = '95.211.175.167:13150'
			#else:
			#	proxy = 'lum-customer-hl_91375698-zone-static:21Corrlinks$@zproxy.lum-superproxy.io:22225'
			p = random.choice(Proxies.objects.filter(idd=s.proxy))
			proxy = f"{p.host}:{str(p.port)}"
			bt = threading.Thread(target=update,args=(str(y),proxy))
			bt.daemon = True
			bt.name = str(y)
			bt.start()
		return [True,str(y)]
	except Exception as e:
		raise e

def start(request): # request receiver
	try:
		res = check_protection(request)
		if res:
			pass
		else:
			return redirect('/viewprotection')
		count = request.GET.get('count','1')
		stop_first()

		resp = generate_threads(int(count))

		print(resp)
		if resp[0]:
			return HttpResponse(json.dumps({
				'status':'runing',
				'message':resp[-1]
			}))
		else:
			return HttpResponse(json.dumps({
			'status':'failed',
			'message':resp[-1]
		}))

	except Exception as e:
		print(str(e))
		return HttpResponse(json.dumps({
			'status':'failed'
		}))


def stop_thread(request):
	res = check_protection(request)
	if res:
		pass
	else:
		return redirect('/viewprotection')
	idd = request.GET.get('idd','')
	count = request.GET.get('count','')
	if idd != '':

		task = Working_threads.objects.filter(task_id = int(idd),status = 'runing')
		if len(task) == 0:
			return HttpResponse(json.dumps({
			'status':'stoped',
			'message':'queue empty'
		}))
		else:
			task = task[0]
		task.stop = True
		task.save()
		return HttpResponse(json.dumps({
			'status':'stoped'
		}))
	else:
		if True:
			if count != '':
				count = int(count)
				tasks = Working_threads.objects.filter(status='runing')
				if count <= len(tasks):
					counter = 0
					while counter < count:
						task = Working_threads.objects.filter(status = 'runing')[0]
						task.stop = True
						task.save()
						counter += 1
					return HttpResponse(json.dumps({
								'status':'stoped',
								'length':str(counter)
							}))
				else:
					for task in tasks:
						task.stop = True
						task.save()
					return HttpResponse(json.dumps({
								'status':'stoped',
								'length':str(count)
							}))





			else:
				task = Working_threads.objects.filter(status = 'runing')
				if len(task) == 0:
					return HttpResponse(json.dumps({
					'status':'stoped',
					'message':'queue empty'
				}))
				else:
					task = task[0]
					task.stop = True
					task.save()
					return HttpResponse(json.dumps({
					'status':'stoped',
					'idd' : str(idd)
				}))
		else:
			pass


			return HttpResponse(json.dumps({
				'status':'stoped None'
			}))

def stop_first():
	tasks = Working_threads.objects.all()
	for x in tasks:
		x.delete()

####


### verifier ###


def check_runing_ver(idd):
	for x in threading.enumerate():
		if x.name == 'v-'+str(idd):
			return True
		else:
			return False

def clear_threads_ver(count):
	lst = [int(x) for x in range(count)]
	tasks  =  Working_threads.objects.all()
	for task in tasks:
		if task.task_id in lst:
			pass
		else:
			if check_runing_ver(task.task_id):
				pass
			else:
				task.status = 'stopped'
			task.stop = True
			task.save()

def update_verify(inmates,idd,proxy):
	global stop
	global message

	tasks = verify_threads.objects.filter(task_id=int(idd))
	if len(tasks) == 0:
		task = verify_threads.objects.create(task_id = int(idd),status='stopped',stop=False)
		task.save()
	else:
		task = tasks[0]
		if check_runing_ver(task.task_id):
			pass
		else:
			task.status = 'stopped'
			task.stop = False
			task.save()
	verify_id_list.append(int(idd))
	while True:
		time.sleep(0.3)
		for  inm in inmates:
			task = verify_threads.objects.filter(task_id = int(idd))[0]

			if task.stop:
				task.status = 'stopped'
				task.save()
				break
			else:
				print(inm.inmate_num)
				verify_location(idd,inm.inmate_num,proxy)
		if task.stop:
			task.status = 'stopped'
			task.save()
			break

def reset_verify(request):
	res = check_protection(request)
	if res:
		pass
	else:
		return redirect('/viewprotection')
	nms = Inmate.objects.filter(verified=True)
	for nm in nms:
		nm.verified = False
		nm.save()
	return HttpResponse('success')

def reset_internally_verify():
	nms = Inmate.objects.filter(verified=True)
	for nm in nms:
		nm.verified = False
		nm.save()

def generate_threads_verify(count):
	try:
		#proxies = renew_proxies()
		clear_threads_ver(count)
		fnames = Inmate.objects.filter(verified=False)

		if  len(fnames) != 0:
			pass
		else:
			fnames_test = Inmate.objects.all()
			if len(fnames_test) == 0:
				return [False,'no inmates yet']
			else:
				reset_internally_verify()
				return [False,'rerun in a moment']
		rate = len(fnames) // count
		print(rate)
		for y in range(count):
			if rate == len(fnames):
				print('here')
				partial_list = fnames
			elif y == (count-1):
				partial_list = fnames[rate*y:]
			else:
				partial_list = fnames[rate*y:rate*(y+1)]
			#proxy = '108.59.14.200:13152'
			#proxy = '5.79.66.2:13200'
			s = ProxySet.objects.all()[0]
			p = random.choice(Proxies.objects.filter(idd=s.proxy))
			proxy = f"{p.host}:{str(p.port)}"
			#proxies.remove(proxy)
			bt = threading.Thread(target=update_verify,args=(partial_list,str(y),proxy))
			bt.daemon = True
			bt.name = 'v-'+str(y)
			bt.start()
		return [True,str(y)]
	except Exception as e:
		raise e

def start_verifier(request):
	try:
		res = check_protection(request)
		if res:
			pass
		else:
			return redirect('/viewprotection')
		count = request.GET.get('count','1')
		stop_second()
		resp = generate_threads_verify(int(count))
		#resp = [True,'this test']
		print(resp)
		if resp[0]:
			return HttpResponse(json.dumps({
				'status':'runing',
				'message':resp[-1]
			}))
		else:
			return HttpResponse(json.dumps({
			'status':'failed',
			'message':resp[-1]
		}))

	except Exception as e:
		print(str(e))
		return HttpResponse(json.dumps({
			'status':'failed'
		}))

def stop_thread_verify(request):
	res = check_protection(request)
	if res:
		pass
	else:
		return redirect('/viewprotection')
	idd = request.GET.get('idd','')
	count = request.GET.get('count','')
	if idd != '':
		task = verify_threads.objects.filter(task_id = int(idd),status = 'runing')
		if len(task) == 0:
			return HttpResponse(json.dumps({
			'status':'stoped',
			'message':'queue empty'
		}))
		else:
			task = task[0]
		task.stop = True
		task.save()
		return HttpResponse(json.dumps({
			'status':'stoped'
		}))
	else:

		if True:
			if count != '':
				count = int(count)
				tasks = verify_threads.objects.filter(status = 'runing')
				if count <= len(tasks):
					counter = 0
					while counter < count:
						print('stoping')
						task = verify_threads.objects.filter(status = 'runing')[0]
						task.stop = True
						task.save()
						counter += 1
					return HttpResponse(json.dumps({
							'status':'stoped',
							'length':str(counter)
						}))

				else:
					for task in tasks:
						print('stoping')
						task.stop = True
						task.save()

					return HttpResponse(json.dumps({
							'status':'stoped',
							'length':str(count)
						}))



			else:
				task = verify_threads.objects.filter(status = 'runing')
				if len(task) == 0:
					return HttpResponse(json.dumps({
					'status':'stoped',
					'message':'queue empty'
				}))
				else:
					task = task[0]
					task.stop = True
					task.save()
					return HttpResponse(json.dumps({
					'status':'stoped',
					'idd' : str(idd)
				}))
		else:
			pass


			return HttpResponse(json.dumps({
				'status':'stoped None'
			}))


def stop_second():
	tasks = verify_threads.objects.all()
	for x in tasks:
		x.delete()

####


### worker status getters ###

# delete stopped threads on guesser
def g_threads():
	if len(id_list) == 0:
		return {}
	else:
		ret_dict = {}
		for x in id_list:
			task = Working_threads.objects.filter(task_id = int(x))
			if len(task) == 0:
				continue
				id_list.remove(x)

			else:
				task = task[0]
			if not check_runing(task.task_id) and ('stopped' in task.status and not task.recent) :
				task.delete()
			else:
				ret_dict[str(task.task_id)] = task.status
	return ret_dict


def get_threads(request):
	res = check_protection(request)
	if res:
		pass
	else:
		return redirect('/viewprotection')
	ret_dict = g_threads()
	return HttpResponse(json.dumps({
		'len':str(len(threading.enumerate())),
		'message':ret_dict
	}))

#delete stoped threads on verifier
def g_threads_verify():
	if len(verify_id_list) == 0:
		return {}
	else:
		ret_dict = {}
		for x in verify_id_list:
			task = verify_threads.objects.filter(task_id = int(x))
			if len(task) == 0:
				continue
				verify_id_list.remove(x)

			else:
				task = task[0]
			if not check_runing_ver(task.task_id) and ('stopped' in task.status and not task.recent):
				task.delete()
			else:
				ret_dict[str(task.task_id)] = task.status
	return ret_dict

# get working threads
def get_threads_verify(request):
	res = check_protection(request)
	if res:
		pass
	else:
		return redirect('/viewprotection')
	ret_dict = g_threads_verify()
	return HttpResponse(json.dumps({
		'len':str(len(threading.enumerate())),
		'message':ret_dict
	}))




#########



#### other ###

def get_details_update(request):
	res = check_protection(request)
	if res:
		pass
	else:
		return redirect('/viewprotection')
	names_dict = g_threads()
	inmates_dict = g_threads_verify()
	current = Tracker.objects.all()[0]
	inmates = Inmate.objects.filter(verified = True)

	return HttpResponse(json.dumps({
		'names_proc':str(len(names_dict.keys())),
		'inmates_proc':str(len(inmates_dict.keys())),
		'current_number':str(current.current_number),
		'processed_inmates':str(len(inmates))

	}))

def control_panel(request):
	res = check_protection(request)
	if res:
		pass
	else:
		return redirect('/viewprotection')
	return render(request,'pannel.html')


def reset(request):
	res = check_protection(request)
	if res:
		pass
	else:
		return redirect('/viewprotection')
	inmates = Inmate.objects.filter(filtered=True)
	for inm in inmates:
		inm.filtered = False
		inm.save()

	return HttpResponse(json.dumps({
		'status':'success'
	})) # reset the filtered users

def format_resp_inmate(user):

	ret_dict = {
		'first_name':user.first_name,
		'middle_name':user.middle_name,
		'last_name' :user.last_name,
		'sex' : user.sex,
		'race' : user.race,
		'age' : user.age,
		'inmate_num' : user.inmate_num,
		'facl_code' : user.facl_code,
		'rel_date' : user.relDate,
		'region': user.region,
		'street' : user.street,
		'city': user.city,
		'county' : user.country,
		'state' :user.state,
		'zipCode' : user.zipcode,
		'faclDescription' : user.faclDescription,
		'areaCode' : user.areaCode
	}

	return ret_dict

def print_page(request):
	res = check_protection(request)
	if res:
		pass
	else:
		return redirect('/viewprotection')
	facilities = Facilities.objects.all()
	return render(request,'filterer.html',{
		'facilities':facilities
	}) # show the filter UI


def custom_print(request): # show the custom print UI
	res = check_protection(request)
	if res:
		pass
	else:
		return redirect('/viewprotection')
	return render(request,'custom_filter.html')

####

#### filter functions handling the filters templates #####
### Main filter function ####

def used_list(lst):
	for  x in lst:
		inm = Inmate.objects.filter(inmate_num = x['inmate_num'])[0]
		inm.filtered = True
		inm.lastprinted = timezone.now()
		inm.save()

def filter_ui(request):
	res = check_protection(request)
	if res:
		pass
	else:
		return redirect('/viewprotection')
	if request.method == 'GET':
		name = request.GET.get('name','').upper()
		name_start = request.GET.get('nameStart','').upper()
		surname = request.GET.get('surname','').upper()
		surname_start = request.GET.get('surnameStart','').upper()
		age = request.GET.get('age','')
		date_start = request.GET.get('dateStart','')
		date_start = date_start.replace('-','/')
		print(date_start)
		date_end = request.GET.get('dateEnd','')
		date_end = date_end.replace('-','/')
		print(date_end)
		white = request.GET.get('white','')
		if 'on' in white:
			white = 'white'.lower()
		black = request.GET.get('black','')
		if 'on' in black:
			black = 'black'.lower()
		asian = request.GET.get('asian','')
		if 'on' in asian:
			asian = 'asian'.lower()
		indian = request.GET.get('indian','')
		if 'on' in indian:
			indian = 'American Indian'.lower()
		race = [white,black,asian,indian]
		count = request.GET.get('count','')
		fac_code = request.GET.get('facility','')
		out_c = request.GET.get('out_c','')
		if out_c == '':
			out_c = '1'
		else:
			out_c = str(out_c)
		if '-' in age:
			age_sp = age.split('-')
			age_st = age_sp[0]
			age_end = age_sp[-1]
			age = ''
		else:
			age_st = ''
			age_end = ''
		if name != '':
			if surname != '':
				if age != '':
					users = Inmate.objects.filter(filtered=False,first_name=name,last_name = surname,age=int(age))
					if len(users) != 0:
						ret_list = []
						for user in users:
							ret_list.append(format_resp_inmate(user))
					else:
						return HttpResponse(json.dumps({
							'status':'fail',
							'response':'no inmates found'
						}))
				else:
					users = Inmate.objects.filter(filtered=False,first_name=name,last_name = surname)
					if len(users) != 0:
						ret_list = []
						for user in users:
							ret_list.append(format_resp_inmate(user))
					else:
						return HttpResponse(json.dumps({
							'status':'fail',
							'response':'no inmates found'
						}))
			else:
				if surname_start != '':
					if age != '':
						users = Inmate.objects.filter(filtered=False,first_name=name,last_name__startswith = surname_start,age=int(age))
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
					else:
						users = Inmate.objects.filter(filtered=False,first_name=name,last_name__startswith = surname_start)
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
				else:
					if age != '':
						users = Inmate.objects.filter(filtered=False,first_name=name,age=int(age))
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
					else:
						users = Inmate.objects.filter(filtered=False,first_name=name)
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
		else:
			if name_start != '':
				if surname != '':
					if age != '':
						users = Inmate.objects.filter(filtered=False,first_name__startswith=name_start,last_name = surname,age=int(age))
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
					else:
						users = Inmate.objects.filter(filtered=False,first_name__startswith=name_start,last_name = surname)
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
				else:
					if surname_start != '':
						if age != '':
							users = Inmate.objects.filter(filtered=False,first_name__startswith=name_start,last_name__startswith = surname_start,age=int(age))
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
						else:
							users = Inmate.objects.filter(filtered=False,first_name__startswith=name_start,last_name__startswith = surname_start)
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
					else:
						if age != '':
							users = Inmate.objects.filter(filtered=False,first_name__startswith=name_start,age=int(age))
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
						else:
							users = Inmate.objects.filter(filtered=False,first_name__startswith=name_start)
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
			else:
				if surname != '':
					if age != '':
						users = Inmate.objects.filter(filtered=False,last_name = surname,age=int(age))
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
					else:
						users = Inmate.objects.filter(filtered=False,last_name = surname)
						if len(users) != 0:
							ret_list = []
							for user in users:
								ret_list.append(format_resp_inmate(user))
						else:
							return HttpResponse(json.dumps({
								'status':'fail',
								'response':'no inmates found'
							}))
				else:
					if surname_start != '':
						if age != '':
							users = Inmate.objects.filter(filtered=False,last_name__startswith = surname_start,age=int(age))
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
						else:
							users = Inmate.objects.filter(filtered=False,last_name__startswith = surname_start)
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
					else:
						if age != '':
							users = Inmate.objects.filter(filtered=False,age=int(age))
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
						else:
							users = Inmate.objects.filter(filtered=False)
							if len(users) != 0:
								ret_list = []
								for user in users:
									ret_list.append(format_resp_inmate(user))
							else:
								return HttpResponse(json.dumps({
									'status':'fail',
									'response':'no inmates found'
								}))
		print(race)
		if any(check_list(race)) :
			print('doing')
			ret_list = filter_race(race,ret_list)

		if age_st != '' and age_end != '':
			ret_list = filter_age(age_st,age_end,ret_list)
		else:
			pass

		if fac_code != '':
			ret_list = filter_fac(fac_code,ret_list)
			facl_clean = True
		else:
			facl_clean = False
			pass

		if date_start != '' and date_end != '':
			final_list = filter_list(date_start,date_end,ret_list)
		else:
			final_list = ret_list

		print(len(final_list))

		print('count ' + str(count))

		if count != '':
			count = int(count)
			if len(final_list) <= count:
				pass
			else:
				div_count = count - (count%20)
				if div_count == 0:
					final_list = final_list[:count]
				else:
					final_list = final_list[:div_count]
		else:
			pass

		print(len(final_list))
		if  facl_clean:
			final_list = clean_facl_list(final_list)
		#print(final_list)



		used_list(final_list)
		if out_c == '1':
			resp = format_pdf(final_list)
		elif out_c == '2':
			resp = format_csv(final_list)
		else:
			resp = format_both(final_list)
		return resp
	else:
		return HttpResponse('Use get requests')


def save_time(l):
	for i in l:
		inm = Inmate.objects.filter(inmate_num = i['inmate_num'])
		inm[0].lastprinted = timezone.now()
		inm[0].save()
	

def clean_facl_list(lst):
	l = []
	for dct in lst:
		addr = dct['street'] + ' ' + dct['city'] + ' ' +  dct['state'] + ' ' + dct['zipCode']
		add = Facilities.objects.filter(code=dct['facl_code'])[0]
		if add.address == addr:
			l.append(dct)
		else:
			continue
	return l


def clean_list(lst,key=False):
	d = {}
	for dct in lst:
		addr = dct['street'] + ' ' + dct['city'] + ' ' +  dct['state'] + ' ' + dct['zipCode']
		#print(addr)
		if addr in d:
			d[addr].append(dct)
		else:
			d[addr] = [dct]
	l = {}
	for x in d.keys():
		l[x] = len(d[x])
	l1 = {k: v for k, v in sorted(l.items(), key=lambda item: item[1])}
	if len(l1) == 0:
		return []
	else:
		if key:
			return list(l1.keys())[-1]

		else:

			return d[list(l1.keys())[-1]]
# fix db
def get_addresses(requests):
	res = check_protection(requests)
	if res:
		pass
	else:
		return redirect('/viewprotection')
	facs = Facilities.objects.all()
	for fac in facs:
		print(fac.code)
		inmates = Inmate.objects.filter(facl_code=fac.code)
		dcts = [format_resp_inmate(x) for x in inmates]
		addr = clean_list(dcts,key=True)
		fac.address = addr
		fac.save()
		print('here')
		print(f'for {fac.code} ==> {addr}')

	return HttpResponse('success')




def reformat_code(code):
	if '-' in code:
		if len(code) == 9:
			return [True]
		else:
			return [False,None]
	else:
		if len(code) == 8:
			code = code[:5] + '-' + code[5:]
			return [False,code]
		else:
			return [False,None]


def custom_check(code):
	print('getting custom')
	inmateNumber = code
	first_query = 'https://www.bop.gov/PublicInfo/execute/inmateloc?todo=query&output=json&inmateNum={0}&inmateNumType=IRN'
	second_query = 'https://www.bop.gov/PublicInfo/execute/phyloc?todo=query&output=json&code={0}'
	#proxy = '5.79.66.2:13200'
	s = ProxySet.objects.all()[0]
	p = random.choice(Proxies.objects.filter(idd=s.proxy))
	proxy = f"{p.host}:{str(p.port)}"
	sess_proxy = {'http':'http://'+proxy,'https':'https://'+proxy}
	json_resp = run_req(first_query.format(inmateNumber),proxy=sess_proxy)
	data = json.loads(json_resp.content.decode())
	data_list = data['InmateLocator']
	if len(data_list) == 0:
		print('data_list is null')
		return False
	else:
		dt_set = data_list[0]
		print(dt_set)
		try:
			if dt_set['projRelDate'] == '':
				print('date')
				return False
			else:
				splited = dt_set['projRelDate'].split('/')
				now = datetime.datetime.now()
				if int(splited[-1]) > now.year:
					pass
				elif int(splited[-1]) == now.year :
					if int(splited[0]) > now.month:
						pass
					elif int(splited[0]) == now.month:
						if int(splited[1]) > now.day:
							pass
						else:
							print('day')
							return False

					else:
						print('month')
						return False

				else:
					print('year')
					return False

		except Exception as e:
			print(str(e))
			return False

		addr_resp = run_req(second_query.format(dt_set['faclCode']),proxy = sess_proxy)
		print('here')
		addr_data = json.loads(addr_resp.content.decode())
		addr_dict = addr_data['Addresses'][0]

		print('saving')


		inmate = Inmate.objects.create(
			first_name = dt_set['nameFirst'],
			middle_name = dt_set['nameMiddle'],
			last_name = dt_set['nameLast'],
			sex = dt_set['sex'],
			race = dt_set['race'],
			age = int(dt_set['age']),
			inmate_num = dt_set['inmateNum'],
			facl_code = dt_set['faclCode'],
			relDate = dt_set['projRelDate'],
			region = addr_dict['region'],
			street = addr_dict['street'] + ' ' + addr_dict['street2'],
			city = addr_dict['city'],
			country = addr_dict['county'],
			state = addr_dict['state'],
			zipcode = addr_dict['zipCode'],
			faclDescription = addr_dict['faclTypeDescription'],
			areaCode  = addr_dict['areaCode'],
			lastprinted = timezone.now(),
			lastverified= timezone.now()
		)
		inmate.save()

		return inmate
	return False

# handle custom filters

def verify_cust(data,idd):
	splited = data.split(',')
	print(len(splited))
	print(splited)
	inmates = []
	for x in splited:
		resp = reformat_code(x)
		if resp[0]:
			pass
		else:
			x = resp[-1]
			if x:
				pass
			else:
				continue
		inm = Inmate.objects.filter(inmate_num = x)
		if len(inm) == 0:
			resp = custom_check(x)
			#input(resp)
			if resp:
				inm = [resp]
			else:

				continue
		else:
			resp = custom_check(x)
			#input(resp)
			if resp:
				inm = [resp]
			else:

				continue
		inmates.append(format_resp_inmate(inm[0]))

	#input(inmates)

	c = custInm.objects.filter(cust_id=idd)[0]
	c.done = True
	c.inmates = json.dumps(inmates)
	c.save()


def custom_filter(requests):
	#res = check_protection(requests)
	#if res:
	#	pass
	#else:
	#	return redirect('/viewprotection')


	if requests.method == 'GET':
		data = requests.GET.get('inmates','')
		out_c = requests.GET.get('out_c','')
		if out_c == '':
			out_c = '1'
		else:
			out_c = str(out_c)
		print(data)
		if data != '':
			idd = str(random.randrange(1000,999999))
			c = custInm.objects.create(cust_id=idd,out_c = out_c)

			c.save()
			t = threading.Thread(target=verify_cust,args=(data,idd,))
			t.daemon = True
			t.start()
			return HttpResponse(json.dumps({'id':idd,'message':'Processing your file please wait'}))


		else:
			return HttpResponse(json.dumps({'message':'empty'}))
	else:
		return HttpResponse(json.dumps({'message':'Use GET requests'}))

def check_status_cust(request):
	#res = check_protection(request)
	#if res:
	#	pass
	#else:
	#	return redirect('/viewprotection')
	idd = request.GET.get('id','')
	if idd != '':
		c = custInm.objects.filter(cust_id=idd)
		if len(c) == 0:
			return HttpResponse(json.dumps({'message':'empty'}))
		else:
			c = c[0]
			if c.done:
				return HttpResponse(json.dumps({'message':'done'}))
			else:
				return HttpResponse(json.dumps({'message':'not done yet'}))
	else:
		return HttpResponse(json.dumps({'message':'empty'}))

def get_file(request):
	#res = check_protection(request)
	#if res:
	#	pass
	#else:
	#	return redirect('/viewprotection')
	idd = request.GET.get('id','')
	if idd != '':
		c = custInm.objects.filter(cust_id=idd)
		if len(c) == 0:
			return HttpResponse(json.dumps({'message':'empty'}))
		else:
			c = c[0]
			if c.done:
				inmates = json.loads(c.inmates)
				out_c = c.out_c
				if out_c == '1':
					resp = format_pdf(inmates)
				elif out_c == '2':
					resp = format_csv(inmates)
				else:
					resp = format_both(inmates)
				c.delete()

				return resp
			else:
				return HttpResponse(json.dumps({'message':'not done yet'}))


	else:
		return HttpResponse(json.dumps({'meessage':'empty'}))


#####


def check_list(lst):
	resp = []
	for x in lst:
		if '' == x:
			resp.append(False)
		else:
			resp.append(True)
	return resp

def filter_fac(code,lst):
	ret_list = []
	for inm in lst:
		if  code == inm['facl_code']:
			ret_list.append(inm)
		else:
			pass
	return ret_list


def filter_race(race,lst):
	ret_list = []
	for inm in lst:
		if inm['race'].lower().strip() in race:
			ret_list.append(inm)
		else:
			pass
	return ret_list


def filter_age(st,end,lst):
	ret_list  = []
	for inm in lst:
		age = int(inm['age'])
		if  int(st) <= age and int(end) >= age:
			ret_list.append(inm)
		else:
			pass

	return ret_list

def filter_list(st,end,lst):
	if '/' not in st and '/' not in end:
		return False
	final_list = []
	for inm in lst:
		try:

			date_splited = [int(x) for x in list(inm['rel_date'].split('/'))]
			st_splited = [int(x) for x in list(st.split('/'))]
			end_splited = [int(x) for x in list(end.split('/'))]
			if date_splited[-1] >= st_splited[-1] and date_splited[-1] <= end_splited[-1]:
				if date_splited[0] >= st_splited[0] and date_splited[0]  <= end_splited[0]:
					if date_splited[-2]  >= st_splited[-2] and date_splited[-2] <= end_splited[-2]:
						final_list.append(inm)
					else:
						continue
				else:
					continue
			else:
				continue
		except:
			continue
	return final_list









#######

#### pdf generation functions ####

#specs = labels.Specification(215.9, 279.4, 3, 10, 66.68, 25.4, corner_radius=2)
specs = labels.Specification(215.9,279.4,  2, 10, 101.6, 25.4, corner_radius=2,left_padding=5, top_padding=5, bottom_padding=5, right_padding=5, padding_radius=2,
left_margin=4.7625,
right_margin=4.7625,
top_margin=12.7,
bottom_margin=12.7,
row_gap=0
)

main_string = """LAMAR JAMES FORD\n09062041\nFEDERAL PRISON CAMP\nP.O. BOX 1000 DULUTH, MN  55814\n"""
# Create a function to draw each label. This will be given the ReportLab drawing
# object to draw on, the dimensions (NB. these will be in points, the unit
# ReportLab uses) of the label, and the object to render.
def draw_label(label, width, height, obj):
    # Just convert the object to a string and print this at the bottom left of
    # the label.
	splited = str(obj).split('\n')
	starting_range = 35

	counter = 35
	count = 1
	for st in splited:

		if counter == starting_range:
			label.add(shapes.String(8, counter, st, fontName="Helvetica", fontSize=10))
		elif count == len(splited)+1:
			print('here')
			label.add(shapes.Image(24,counter,80,15,'code.jpg'))
		else:
			label.add(shapes.String(8, counter, st, fontName="Helvetica", fontSize=8))

		counter -= 7
		count += 1
		if counter <= 0:
			break
		else:
			continue
	#label.add(shapes.Image(24,counter-7,80,12,'code.jpg'))

def format_message(dct):
	message = dct['first_name'] + ' ' + dct['middle_name'] + ' ' + dct['last_name']  + ' #'+ dct['inmate_num'] + '\n' + dct['faclDescription'] + '\n' + dct['street'] + ' ' + dct['city'] + '\n' +  dct['state'] + ' ' + dct['zipCode']
	return message


def crt(filename,lst):
	# Create the sheet.
	sheet = labels.Sheet(specs, draw_label, border=False)

	#print(lst)
	for i in lst:
		main_string = format_message(i)
		sheet.add_label(main_string)


	sheet.save(filename)

def format_pdf(final_list,file=False):
	filename = str(random.randrange(1000,9999999))+'.pdf'
	crt(filename,final_list)
	if file:
		return filename
	else:
		file_t = open(filename,'rb')
		buffer = file_t.read()
		file_t.close()
		os.remove(filename)
		response = HttpResponse(buffer, content_type='application/pdf')
		response['Content-Disposition'] = 'attachment; filename="' + filename + '"'
		return response


def format_csv(final_list,file=False):
	if file:
		filename = str(random.randrange(1000,9999999))+'.csv'
		file_c = open('{0}'.format(filename),'w')
		writer = csv.writer(file_c)

	else:
		response = HttpResponse(content_type='text/csv')
		response['Content-Disposition'] = 'attachment; filename="data.csv"'
		writer = csv.writer(response)
	if len(final_list) != 0:
		l = list(final_list[0].keys())

		writer.writerow(l)
		for i in final_list:
			res_row = []
			for x in range(len(l)):
				res_row.append(i[l[x]])
			writer.writerow(res_row)


	if file:
		file_c.close()
		return filename
	else:

		return response




def format_both(final_list):
	zf = zipfile.ZipFile('data.zip',mode='w')
	n1 = format_pdf(final_list,file=True)
	n2 = format_csv(final_list,file=True)
	print('writing')
	print(n1)
	print(n2)
	zf.write(n1)
	zf.write(n2)
	zf.close()
	os.remove(n1)
	os.remove(n2)
	buffer = open('data.zip','rb').read()
	response = HttpResponse(buffer, content_type='application/octet-stream')
	response['Content-Disposition'] = 'attachment; filename=data.zip'
	os.remove('data.zip')
	return response






######

#### remove duplicates #####
def remove_dup():
	try:

		while True:
			done = True
			inmates = Inmate.objects.all()
			counter = 0
			for x in inmates:
				counter += 1
				print('processing : {0} / {1}'.format(str(counter),str(len(inmates))))
				lst = Inmate.objects.filter(inmate_num = x.inmate_num)
				if len(lst) > 1:
					for i in lst[1:]:
						i.delete()
					done = False
					break
				else:
					continue
			if done:
				break
			else:
				continue
		return HttpResponse('done')

	except Exception as e:
		raise

# set proxy
def setproxy(request):
	res = check_protection(request)
	if res:
		pass
	else:
		return redirect('/viewprotection')
	num = request.GET.get('num','')
	if num == '':
		return HttpResponse(json.dumps({'result':'failed'}))
	else:
		num = int(num)
		sts = ProxySet.objects.all()
		if len(sts) == 0:
			s = ProxySet.objects.create(proxy=num)
			s.save()
		else:
			s = ProxySet.objects.all()[0]
			s.proxy = num
			s.save()
		return HttpResponse(json.dumps({'result':'success'}))

# view protection checker function

def verify_protection(request):
	if request.method == 'POST':

		password = request.POST.get('password','')

		if  password == '':
			return render(request,'protection.html',{'message':'empty field'})

		usr = ViewProtection.objects.filter(password =  password )
		if len(usr) != 0:
			usr = usr[0]
			if password == usr.password:
				request.session.set_expiry(0)
				request.session['protection'] = usr.password
				return  redirect('/panel')
			else:
				return render(request,'protection.html',{'message':'password is incorrect'})

		else:
			return render(request,'protection.html',{'message':'user not found'})

	else:
		return render(request,'protection.html',{'message':'not valid'})

def check_protection(request):
	password = request.session.get('protection','')
	if password != '':
		return True
	else:
		return False

def view_protection(request):
	return render(request,'protection.html')

def error_404(request,exception):
	res = check_protection(request)
	if res:
		pass
	else:
		return redirect('/viewprotection')
	return HttpResponse('Page not Found')
