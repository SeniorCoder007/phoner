import requests as req
import sys
import json
import pprint

num = sys.argv[-1]
print(num)
first_query = 'https://www.bop.gov/PublicInfo/execute/inmateloc?todo=query&output=json&inmateNum={0}&inmateNumType=IRN'
second_query = 'https://www.bop.gov/PublicInfo/execute/phyloc?todo=query&output=json&code={0}'
pp = pprint.PrettyPrinter()

resp1 = req.get(first_query.format(str(num)))
data = json.loads(resp1.content.decode())
pp.pprint(data)
resp2 = req.get(second_query.format(data['InmateLocator'][0]['faclCode']))
data2 = json.loads(resp2.content.decode())
pp.pprint(data2)
