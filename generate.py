from reportlab.graphics import shapes
from reportlab.pdfgen import canvas
import labels







#buffer = io.BytesIO()
#canva = canvas.Canvas(buffer)
#canva.drawString(100, 100, "Hello world.")
#canva.showPage()
#canva.save()
#return FileResponse(buffer,as_attachment=True,filename='hello.pdf')






specs = labels.Specification(215.9,279.4,  2, 10, 101.6, 25.4, corner_radius=2,left_padding=5, top_padding=5, bottom_padding=5, right_padding=5, padding_radius=2,
left_margin=4.7625,
right_margin=4.7625,
top_margin=12.7,
bottom_margin=12.7,
row_gap=0
)


# Create a function to draw each label. This will be given the ReportLab drawing
# object to draw on, the dimensions (NB. these will be in points, the unit
# ReportLab uses) of the label, and the object to render.
def draw_label(label, width, height, obj):
    # Just convert the object to a string and print this at the bottom left of
    # the label.
	splited = str(obj).split('\n')
	starting_range = 35

	counter = 35
	count = 1
	for st in splited:

		if counter == starting_range:
			label.add(shapes.String(8, counter, st, fontName="Helvetica", fontSize=10))
		elif count == len(splited)+1:
			print('here')
			label.add(shapes.Image(24,counter,80,15,'code.jpg'))
		else:
			label.add(shapes.String(8, counter, st, fontName="Helvetica", fontSize=8))

		counter -= 7
		count += 1
		if counter <= 0:
			break
		else:
			continue
	#label.add(shapes.Image(24,counter-7,80,12,'code.jpg'))


def crt(filename):
	# Create the sheet.
	sheet = labels.Sheet(specs, draw_label, border=False)


	for i in range(20):
		main_string = """LAMAR JAMES FORD #09062041\nFEDERAL PRISON CAMP\nP.O. BOX 1000\nDULUTH, MN  55814"""
		sheet.add_label(main_string)


	sheet.save(filename)


crt('labels4.pdf')
